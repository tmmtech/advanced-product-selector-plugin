== Changelog ==

= 0.1.18 2024-05-02 =

#### Enhancements

* Use standard Action Scheduler interval.
* Update WP-Plugins-Core.

= 0.1.17 2023-07-18 =

#### Enhancements

* Set min PHP version to 7.4.

= 0.1.7 2023-01-29 =

#### Enhancements

* Changelog added.
