# About

A WordPress plugin that extends the default WooCommerce selector for variable products.

------

## Attribute meta data

| Option                      | Description                                                                        |
|-----------------------------|------------------------------------------------------------------------------------|
| `advanced-product-selector` | Contains all plugin data for the attribute (description, terms_order). |

## Attribute term meta data

| Option                      | Description                                                       |
|-----------------------------|-------------------------------------------------------------------|
| `advanced-product-selector` | Contains all plugin data for the attribute term (tooltip, image). |

## Libs

### JS

| Name                                                             | Description              | Installation   |
|------------------------------------------------------------------|--------------------------|----------------|
| [SortableJS](https://sortablejs.github.io/Sortable/)             | Ordering of attributes.  |                |
| [Popper.js](https://popper.js.org/)                              | Popper.js - for tippy.js | @popperjs/core |
| [TippyJS](https://atomiks.github.io/tippyjs/v6/getting-started/) | Tippy.js - Tooltips      | tippy.js       |
| [Pickr](https://simonwep.github.io/pickr/)                       | Color picker.            |                |
