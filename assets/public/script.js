// noinspection JSUnusedLocalSymbols,JSUnresolvedVariable,ES6MissingAwait

/**
 * Contains public JS routines.
 *
 * @package Advanced-Product-Selector
 */
'use strict';

document.addEventListener( "DOMContentLoaded", async function() {
    let aps = window.advancedProductSelector;

    const $variationsForm     = jQuery( '.variations_form' );
    const $resetVariationsBtn = jQuery( '.reset_variations', $variationsForm );

    const variationsTable = document.querySelector( 'table.variations' );

    if ( ! variationsTable ) {
        return;
    }

    const attributes = Object.fromEntries(  // Filters the attributes to only the existing ones on the page.
        Object.entries( aps.attributes ).filter(
            ([key, value]) => document.querySelector(`select#${key}`)
        )
    );

    // Adds a popup container to the body.
    document.body.insertAdjacentHTML( 'beforeend', `
        <div class="aps-popup">
            <div class="aps-popup-content"></div>
        </div>
    `);

    const apsPopup        = document.querySelector( '.aps-popup' );
    const apsPopupContent = apsPopup.querySelector( '.aps-popup-content' );

    /**
     * Resets all attributes.
     */
    function resetAllAttributes() {
        $resetVariationsBtn.trigger( 'click' );
    }

    /**
     * Selects an option.
     *
     * @param termSlug
     * @param attrSlug
     * @param {Boolean} resetAllOtherOptions Whether to reset all other options.
     */
    function selectOption( termSlug, attrSlug, resetAllOtherOptions = false ) {
        if ( resetAllOtherOptions ) {
            resetAllAttributes();
        }

        const select = document.querySelector( `select#${attrSlug}` );
        select.value = termSlug;
        jQuery( select ).trigger( 'change' );
    }

    /**
     * Returns selected options (attributes + non-attributes).
     *
     * Example:
     *
     * {
     *  'pa_brand': 'zara',
     *  'pa_color': 'red',
     *  'logo':     'Yes'
     * }
     *
     * @return {Object} An object of selected options (attributes + non-attributes).
     */
    function getSelectedOptions() {
        const selects = variationsTable.querySelectorAll( 'select' );

        let selectedOptions = {};

        for ( const select of selects ) {
            const value = select.value;
            if ( value ) {
                selectedOptions[ select.getAttribute( 'name' ).replace( /^attribute_/g, '' ) ] = value;
            }
        }

        return selectedOptions;
    }

    /**
     * Unselected attributes list.
     *
     * @return {String[]} The list of attribute no option for which were selected yet.
     */
    function getUnselectedAttributesList() {
        let unselectedAttributes = [];
        for ( const attributeSlug in attributes ) {
            if ( ! document.querySelector( `select#${attributeSlug}` ).value ) {
                unselectedAttributes.push( attributeSlug );
            }
        }
        return unselectedAttributes;
    }

    /**
     * Returns the name of the next attribute.
     *
     * @param {String} currentAttribute The slug of the current attribute.
     *
     * @return {String|Boolean} The name of the next attribute or false if not next.
     */
    function getTheNextAttributeName( currentAttribute ) {
        const attributesList = Object.keys( attributes );
        const indexToGet = attributesList.indexOf( currentAttribute ) + 1;

        return attributesList.length >= indexToGet + 1 ? attributesList[indexToGet] : false;
    }

    function disableNextAttributesForStepByStep() {
        if ( // Step by step: show disabled the next selects without all in one modal.
            aps.settings.selectAttributesStepByStep &&
            ! aps.settings.showAllAttributesInOneModal
        ) {
            const unselectedAttributes = getUnselectedAttributesList();
            const allUnselectedAttributesExceptTheFirstOne = unselectedAttributes.slice(1);

            Object.keys( attributes ).forEach( attributeSlug => {
                const select    = variationsTable.querySelector( `select#${attributeSlug}` );
                select.disabled = allUnselectedAttributesExceptTheFirstOne.includes( attributeSlug );

                /**
                 * Adds the text for attributes empty options like "Please choose Color first" for disabled options.
                 */
                if ( unselectedAttributes.length ) {
                    const attrThatShouldBeSelectedNext     = unselectedAttributes[0];
                    const attrThatShouldBeSelectedNextName = document.querySelector( `label[for="${attrThatShouldBeSelectedNext}"]` ).innerText;

                    const noOptionSelect = select.querySelector( 'option[value=""]' );
                    if ( attributeSlug !== attrThatShouldBeSelectedNext ) {
                        noOptionSelect.setAttribute( 'data-aps-initial-text', noOptionSelect.innerText );
                        noOptionSelect.innerText = aps.txt.firstChooseAttr.replace( '{}', attrThatShouldBeSelectedNextName );
                    } else {
                        const initialText = noOptionSelect.getAttribute( 'data-aps-initial-text' );
                        if ( initialText ) {
                            noOptionSelect.innerText = initialText;
                        }
                    }
                }
            } );
        }
    }
    disableNextAttributesForStepByStep();

    $variationsForm.on( 'reset_data', function() {
        disableNextAttributesForStepByStep();
    } );

    /**
     * Option to click when the render finishes.
     *
     * Usually, a single option for wizard-style.
     */
    let optionToClickOnRenderFinish;

    /**
     * The last attr block that was clicked (was clicked either one of options within, or pagination) (no matter automatically or manually).
     */
    let lastClickedAttrBlock;

    /**
     * The last attr block that was manually clicked (was clicked either one of options within, or pagination).
     */
    let lastManuallyClickedAttrBlock;

    /**
     * Viewport top position to the last manually clicked option's attribute heading.
     */
    let lastClickedAttrHeadingViewportTop;

    /**
     * Select next attribute button.
     *
     * @type {Element|Boolean}
     */
    let selectNextAttrBtn;

    /**
     * Renders popup content for the particular attribute.
     *
     * @param {String} renderAttributeSlug E.g. "pa_color"
     * @param {Number} pageNum             Page for which to get the terms.
     * @param {Boolean} rerenderOnlyTarget Whether to rerender only the target attribute block (used for pagination)
     * @param {Boolean} onProgressBarClick Whether to rerender on progress bar click.
     */
    function renderPopupContent( renderAttributeSlug, pageNum = 1, rerenderOnlyTarget = false, onProgressBarClick = false ) {
        optionToClickOnRenderFinish = false;
        if ( selectNextAttrBtn ) {
            selectNextAttrBtn.remove();
            selectNextAttrBtn = false;
        }

        disableNextAttributesForStepByStep();

        /**
         * Gets element scrolling position
         *
         * @param {Element} elem Attributes block.
         * @return {number}
         */
        function getElementScrollingPosition( elem ) {
            const scrollingOffset = 30;
            return elem.offsetTop - scrollingOffset;
        }

        /**
         * Scrolls to the previous popup scrolling position.
         *
         * Scrolls to exactly the same position of the last manually clicked attribute, relative to the viewport.
         * (i.e. the heading of it will be at the same place).
         *
         * Used to fix the bug when the block re-rendering shows the first (top) attribute
         * when show all in one modal option is enabled.
         *
         * @param {Number} attrBlockNum Number of the attribute block to scroll to.
         */
        function scrollToThePreviousPopupPosition( attrBlockNum ) {
            const attrBlock = document.querySelector( `.aps-attr-block[data-attr-block-num="${attrBlockNum}"]` );
            const attrBlockHeading = attrBlock.querySelector( '.aps-attr-heading' );

            apsPopupContent.scrollTo( {
                top: attrBlockHeading.getBoundingClientRect().top - lastClickedAttrHeadingViewportTop
            } );
        }

        /**
         * Scrolls to the next attribute block.
         *
         * Used when "show all attributes in one modal" is enabled and plus wizard-style or step-by-step.
         *
         * @param {Number} attrBlockNum Number of the attribute block to scroll to.
         */
        function scrollToTheNextAttributeBlock( attrBlockNum ) {
            const attrBlock     = document.querySelector( `.aps-attr-block[data-attr-block-num="${attrBlockNum}"]` );
            const attrBlockName = attrBlock.getAttribute( 'data-attr-block' );

            const unselectedAttributes = getUnselectedAttributesList();

            if ( unselectedAttributes.length ) {
                const nextAttributeName = getTheNextAttributeName( attrBlockName );
                const nextAttrName      = unselectedAttributes.includes( nextAttributeName ) ? nextAttributeName : unselectedAttributes[0];
                const nextAttrBlock     = document.querySelector( `.aps-attr-block[data-attr-block="${nextAttrName}"]` );

                apsPopupContent.scrollTo( {
                    top: getElementScrollingPosition( nextAttrBlock ),
                    behavior: 'smooth'
                } );
            }
        }

        if ( ! rerenderOnlyTarget ) {
            /**
             * Adds progress bar for modal only when:
             *
             * 1. Not show all stacked in the same modal, and
             * 2. Either select attributes step by step, or wizard style.
             *
             * @return {String}
             */
            function getProgressBarContent() {
                let html = '';

                if (
                    ! aps.settings.showAllAttributesInOneModal &&
                    ( aps.settings.selectAttributesStepByStep || aps.settings.automaticallyShowTheNextAttributeModal )
                ) {
                    function getProgressBarEntitiesHTML() {
                        let html = '';

                        Object.keys( attributes ).forEach( ( attributeSlug, i ) => {
                            const attributeName = document.querySelector( `label[for="${attributeSlug}"]` ).innerText;
                            const select        = variationsTable.querySelector( `select#${attributeSlug}` );

                            function getClasses() {
                                let classes = [];

                                if ( select.value ) {
                                    classes.push( 'selected' );
                                }

                                if ( renderAttributeSlug === attributeSlug ) {
                                    classes.push( 'current' );
                                }

                                if ( aps.settings.customProgressBarArrow ) {
                                    classes.push( 'custom-arrow-set' );
                                }

                                return classes.join( ' ' );
                            }

                            function getArrowImageHTML() {
                                let html = '';
                                if ( aps.settings.customProgressBarArrow ) {
                                    html += aps.settings.customProgressBarArrow;
                                }
                                return html;
                            }

                            html += `
                                <div data-attribute="${attributeSlug}" class="aps-attribute-entity ${getClasses()}">
                                    <span>${attributeName}</span>
                                    ${getArrowImageHTML()}
                                </div>
                            `;
                        } );

                        return html;
                    }

                    html += `<div class="aps-progress-bar">${getProgressBarEntitiesHTML()}</div>`;
                }

                return html;
            }

            apsPopupContent.innerHTML = `
                <button type="button" class="close aps-close-btn" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                ${getProgressBarContent()}
            `;

            // Add progress bar entities events.

            apsPopupContent.querySelectorAll( '.aps-attribute-entity' ).forEach( entity => {
                entity.addEventListener( 'click', function() {
                    const attribute = entity.getAttribute( 'data-attribute' );
                    renderPopupContent( attribute, 1, false, true );
                } );
            } );

            apsPopupContent.querySelector( '.aps-close-btn' ).addEventListener( 'click', closePopup );
        }

        const select = document.querySelector( `select#${renderAttributeSlug}` );

        function showTheNextAttributeModal( attributeSlug ) {
            const unselectedAttributes = getUnselectedAttributesList(),
                nextAttributeName = getTheNextAttributeName( attributeSlug );
            if ( unselectedAttributes.length ) {
                renderPopupContent( unselectedAttributes.includes( nextAttributeName ) ? nextAttributeName : unselectedAttributes[0] );
            } else {
                closePopup();
            }
        }

        const attributesToDisplayTheBlocksFor = rerenderOnlyTarget ? [ renderAttributeSlug ] :
            aps.settings.showAllAttributesInOneModal ?
                [ ...Object.keys( attributes ) ] : [ renderAttributeSlug ];

        attributesToDisplayTheBlocksFor.forEach( ( attributeSlug, i ) => {
            const attributeName = document.querySelector( `label[for="${attributeSlug}"]` ).innerText;
            const attributeData = aps.attributes[ attributeSlug ];
            const select        = variationsTable.querySelector( `select#${attributeSlug}` );

            /**
             * Returns filtered and sorted terms.
             *
             * @return {Object}
             */
            function getTerms() {
                /**
                 * Filter out hidden terms.
                 *
                 * And sort by making unfiltered terms to be at the beginning.
                 */
                const terms = Object.keys( attributeData.terms ).filter( termSlug => {
                    return aps.settings.howToTreatUnavailableAttributeCombinations !== 'hide' ||
                        select.querySelector(`option[value="${termSlug}"]` );
                } ).reduce( ( result, key ) => {
                    result[ key ] = attributeData.terms[ key ];
                    return result;
                }, {} );

                /**
                 * Sorts the terms (first available, then unavailable (grayed-out or disabled).
                 */
                let availableTerms  = [];
                let otherTerms      = [];
                for ( const termSlug in terms ) {
                    const isOptionShown = select.querySelector( `option[value="${termSlug}"]` );
                    if ( isOptionShown ) {
                        availableTerms.push( termSlug );
                    } else {
                        otherTerms.push( termSlug );
                    }
                }
                return availableTerms.concat( otherTerms ).reduce( ( result, key ) => {
                    result[ key ] = attributeData.terms[ key ];
                    return result;
                }, {} );
            }
            const terms = getTerms();

            const totalTerms = Object.keys( terms ).length,
                termsPerPage = attributeData.num_per_page;
            let maxPage;

            /**
             * Returns terms' meta-data.
             *
             * - availableTermsCount {Number}
             * - availableTermsList  {String{}} Slugs.
             * - activeTermPageNum   {Number}   0 if no active term, or page num on which it's located.
             * - activeTermName      {String}   Name of the active term. Empty string if no active term.
             *
             * @return {Object} Terms meta-data.
             */
            function getTermsMetaData() {
                let availableTermsCount = 0;
                let availableTermsList  = [];
                for ( const termSlug of Object.keys( terms ) ) {
                    const isOptionShown = select.querySelector( `option[value="${termSlug}"]` );
                    if ( isOptionShown ) {
                        availableTermsCount++;
                        availableTermsList.push( termSlug );
                    }
                }

                /**
                 * Open the page with the currently active term.
                 */
                let activeTermPageNum = 0;
                if ( select.value ) {
                    const termPosition = Object.keys( terms ).indexOf( select.value ) + 1;
                    activeTermPageNum = Math.ceil( termPosition / termsPerPage );
                }

                return {
                    availableTermsCount: availableTermsCount,
                    availableTermsList: availableTermsList,
                    activeTermPageNum: activeTermPageNum,
                    activeTermName:  select.value
                };
            }
            const termsMetaData = getTermsMetaData();

            if ( ! rerenderOnlyTarget && termsMetaData.activeTermPageNum ) {
                pageNum = termsMetaData.activeTermPageNum;
            }

            /**
             * Open the page with the currently active term.
             */
            if ( ! rerenderOnlyTarget && select.value ) {
                const termPosition = Object.keys( terms ).indexOf( select.value ) + 1;
                pageNum = Math.ceil( termPosition / termsPerPage );
            }

            if ( -1 === termsPerPage ) {
                // If there is a reminder from the division, it's another page.
                maxPage = Math.floor( totalTerms / termsPerPage ) + (totalTerms % termsPerPage ? 1 : 0);
            }

            /**
             * Maybe returns HR separator.
             *
             * @return {string}
             */
            function maybeGetSeparator() {
                return i > 0 ? '<hr class="aps-blocks-separator">' : '';
            }

            /**
             * Returns terms HTML for the page.
             *
             * @return {String} Terms HTML.
             */
            function getTermsHTML() {
                let html = '';

                let termsForThisPage;
                if ( -1 !== termsPerPage ) {
                    termsForThisPage = Object.keys( terms ).splice( ( pageNum - 1 ) * termsPerPage, termsPerPage )
                        .reduce( ( result, key ) => {
                            result[ key ] = terms[ key ];
                            return result;
                        }, {} );
                } else {
                    termsForThisPage = terms
                }

                /**
                 * Returns description HTML.
                 *
                 * @param {String} descriptionHTML
                 *
                 * @return {String} Description block HTML.
                 */
                function getDescriptionHTML( descriptionHTML ) {
                    if ( descriptionHTML ) {
                        return `<div class="aps-option-description">${descriptionHTML}</div>`;
                    } else {
                        return '';
                    }
                }

                /**
                 * Returns price HTML for the term.
                 *
                 * @param {String} termSlug
                 *
                 * @return {String} Price HTML.
                 */
                function getPriceHTML( termSlug ) {

                    const decimalSep  = aps.pricesTemplate.decimal_sep;
                    const thousandSep = aps.pricesTemplate.thousand_sep;
                    const decimalsNum = aps.pricesTemplate.num_decimals;

                    /**
                     * Formats the price.
                     *
                     * @param {Number} price Price to format.
                     *
                     * @return {String} Formatted price.
                     */
                    function formatPrice( price ) {
                        return price.toFixed( decimalsNum ).replace( '.', decimalSep ).replace( /\d(?=(\d{3})+\.)/g, `$&${thousandSep}` );
                    }

                    if ( attributeData.show_price_per_attribute ) {
                        const prices = getAttributesTermPrices( attributeSlug, termSlug );

                        if ( false !== prices ) {
                            const onePrice = prices.min === prices.max;

                            let termPrice;

                            if ( onePrice ) {
                                termPrice = aps.pricesTemplate.one
                                    .replace( `12${decimalSep}34`, formatPrice( prices.min ) );
                            } else {
                                termPrice = aps.pricesTemplate.two
                                    .replace( `12${decimalSep}34`, formatPrice( prices.min ) )
                                    .replace( `98${decimalSep}76`, formatPrice( prices.max ) );
                            }

                            return `<div class="aps-option-price">${termPrice}</div>`;
                        } else {
                            return '';
                        }
                    } else {
                        return '';
                    }
                }

                for ( const termSlug in termsForThisPage ) {
                    const termData = termsForThisPage[ termSlug ];

                    const wrap     = document.createElement( 'div' );
                    wrap.innerHTML = jQuery( select ).data( 'attribute_html' ); // Complete HTML covering all options.
                    const doesOptionExistForThisProduct = wrap.querySelector( `option[value="${termSlug}"]` );
                    wrap.remove();

                    const isOptionShown = select.querySelector( `option[value="${termSlug}"]` );
                    const isRecommended = attributeData.recommended_terms.indexOf( termSlug ) !== -1;

                    if (
                        doesOptionExistForThisProduct &&
                        ( isOptionShown || aps.settings.howToTreatUnavailableAttributeCombinations !== 'hide' )
                    ) {
                        const optionName = doesOptionExistForThisProduct.innerText;

                        /**
                         * Returns recommended block HTML.
                         *
                         * @param {String} recommendedTextHTML HTML of recommended block text.
                         *
                         * @return {String} Recommended block HTML.
                         */
                        function recommendedBlock( recommendedTextHTML ) {
                            if ( isRecommended ) {
                                return `<div class="aps-option-recommended-block">${recommendedTextHTML}</div>`;
                            } else {
                                return '';
                            }
                        }

                        /**
                         * Image styles.
                         *
                         * @return {String} Image styles.
                         */
                        function getImageStyle() {
                            let rules = [];

                            if (
                                'over-the-image' === aps.settings.nonAvailableOptionsTextPlacement &&
                                ! isOptionShown &&
                                attributeData.text_for_non_available_options
                            ) {
                                rules.push( `--aps-non-available-option-text: '${attributeData.text_for_non_available_options}'` );
                            }

                            return `style="${rules.join( ';' )};"`;
                        }

                        /**
                         * Returns hover image HTML.
                         *
                         * @param {Boolean|String} imageHTML
                         *
                         * @return {String} Image block HTML.
                         */
                        function getHoverImageHTML( imageHTML ) {
                            if ( imageHTML ) {
                                return `
                                    <div class="aps-option-image aps-option-hover-image" ${getImageStyle()}>
                                        <div class="aps-img-container">${imageHTML}</div>
                                    </div>
                                `;
                            } else {
                                return '';
                            }
                        }

                        /**
                         * Returns image HTML.
                         *
                         * @param {Boolean|String} imageHTML
                         *
                         * @return {String} Image block HTML.
                         */
                        function getImageHTML( imageHTML ) {
                            if ( imageHTML ) {
                                return `
                                    <div class="aps-option-image" ${getImageStyle()}>
                                        <div class="aps-img-container">${imageHTML}</div>
                                    </div>
                                `;
                            } else {
                                return '';
                            }
                        }

                        /**
                         * Returns text for non-available options.
                         *
                         * @return {String} HTML.
                         */
                        function getNonAvailableOptionTextBlock() {
                            if (
                                'below-the-image' === aps.settings.nonAvailableOptionsTextPlacement &&
                                ! isOptionShown &&
                                attributeData.text_for_non_available_options
                            ) {
                                return `<div class="aps-non-available-option-text-block">${attributeData.text_for_non_available_options}</div>`;
                            } else {
                                return '';
                            }
                        }

                        function getClasses() {
                            let classes = [];

                            // Is active class.
                            if ( termSlug === select.value ) {
                                classes.push( 'active' );
                            }

                            if ( ! isOptionShown ) {
                                if ( 'gray-out' === aps.settings.howToTreatUnavailableAttributeCombinations ) {
                                    classes.push( 'grayed-out' );
                                } else if ( 'disable' === aps.settings.howToTreatUnavailableAttributeCombinations ) {
                                    classes.push( 'disabled' );
                                }
                            }

                            if ( ! termData.image ) {
                                classes.push( 'no-image' );
                            }

                            return classes.join( ' ' );
                        }

                        function getStyles() {
                            let rules = [];

                            if ( isRecommended ) { // Recommended border color.
                                rules.push( `box-shadow: 1px 0 10px 0 ${termData.recommended_border_color}` );
                            }

                            return `style="${rules.join( ';' )};"`;
                        }

                        html += `
                            <div class="aps-option ${getClasses()}" data-slug="${termSlug}" data-name="${optionName}" ${getStyles()}>
                                ${recommendedBlock( termData.recommended_text )}
                                ${getHoverImageHTML( termData.hoverImage )}
                                ${getImageHTML( termData.image )}
                                ${getNonAvailableOptionTextBlock()}
                                ${getDescriptionHTML( termData.description )}
                                ${getPriceHTML( termSlug )}
                                <div class="aps-option-heading">
                                    ${optionName}
                                </div>
                            </div>
                        `;
                    }
                }
                document.querySelector( 'option[value="blue"]' );
                return html;
            }

            function getPaginationHTML() {
                if ( -1 !== termsPerPage && totalTerms > termsPerPage ) {
                    /*
                     * Adds pagination HTML.
                     */
                    let previousPage = 1 === pageNum ? 1 : pageNum - 1,
                        nextPage = pageNum === maxPage ? maxPage : pageNum + 1;

                    const PAGES_VIEW_RANGE = 3;

                    // Adds a new one.

                    let paginationHTML = `
                        <div class="aps-pagination-container">
                            <div class="aps-pagination">
                                <a data-page="${previousPage}"rel="prev" class="aps-pagination-element">${aps.txt.paginationPrevious}</a>
                    `;

                    let page = 1,
                        wereThreeDotsAddedStart = false,
                        wereThreeDotsAddedEnd = false;

                    function printPage() {
                        if ( page === pageNum ) {
                            paginationHTML += `<a data-page="${page}" class="aps-pagination-element aps-pagination-digit active">${page}</a>`;
                        } else {
                            paginationHTML += `<a data-page="${page}" class="aps-pagination-element aps-pagination-digit">${page}</a>`;
                        }
                    }

                    function printThreeDots() {
                        paginationHTML += `<span class="aps-pagination-dots">...</span>`;
                    }

                    for ( let remainedTerms = totalTerms; remainedTerms > 0; remainedTerms -= termsPerPage ) {
                        const isPageInStartRange = page <= PAGES_VIEW_RANGE,
                            isPageAroundCurrentPage = page >= pageNum - PAGES_VIEW_RANGE && page <= pageNum + PAGES_VIEW_RANGE,
                            isPageInEndRange = page > maxPage - PAGES_VIEW_RANGE;

                        if ( isPageInStartRange || isPageAroundCurrentPage || isPageInEndRange ) {
                            printPage();
                        } else {
                            if ( page < pageNum && ! wereThreeDotsAddedStart ) {
                                printThreeDots();
                                wereThreeDotsAddedStart = true;
                            }
                            if ( page > pageNum && ! wereThreeDotsAddedEnd ) {
                                printThreeDots();
                                wereThreeDotsAddedEnd = true;
                            }
                        }

                        page++;
                    }

                    paginationHTML += `
                                <a data-page="${nextPage}" rel="next" class="aps-pagination-element">${aps.txt.paginationNext}</a>
                            </div>
                        </div>
                    `;

                    return paginationHTML;
                } else {
                    return '';
                }
            }

            function getDescriptionHTML() {
                let html = '';
                if ( aps.attributes[attributeSlug].description ) {
                    html += `<div class="aps-attr-description">
                        ${aps.attributes[attributeSlug].description}
                    </div>`;
                }
                return html;
            }

            function addSelectNextAttributeBlock() {
                function getSelectNextAttributeHeadingHTML() {
                    if ( aps.settings.attributesStepByStepSelectionText ) {
                        return `
                            <div class="aps-select-next-attribute-heading">
                                ${aps.settings.attributesStepByStepSelectionText}
                            </div>
                        `;
                    } else {
                        return '';
                    }
                }

                function getAttributeNextBtnText() {
                    if ( getUnselectedAttributesList().length ) {
                        return aps.txt.next;
                    } else {
                        return aps.txt.finish;
                    }
                }

                attrBlock.insertAdjacentHTML( 'beforeend', `
                    <div class="aps-select-next-attribute-block">
                        ${getSelectNextAttributeHeadingHTML()}
                        <div class="aps-select-next-attribute-button">
                            ${getAttributeNextBtnText()}
                        </div>
                    </div>
                ` );

                // Select next attribute button click.

                selectNextAttrBtn = document.querySelector( '.aps-select-next-attribute-button' );
                selectNextAttrBtn.addEventListener( 'click', function() { // Selects the next attribute that is unselected.
                    if ( aps.settings.showAllAttributesInOneModal ) { // Remember the position for the restoration on "scrollToThePreviousPopupPosition()" call further.
                        const lastManuallyClickedOptionAttrHeading = selectNextAttrBtn.closest( '.aps-attr-block' ).querySelector( '.aps-attr-heading' );
                        lastClickedAttrHeadingViewportTop = lastManuallyClickedOptionAttrHeading.getBoundingClientRect().top;
                    }

                    showTheNextAttributeModal( attributeSlug );

                    if ( aps.settings.showAllAttributesInOneModal ) {
                        scrollToTheNextAttributeBlock( attrBlockNum );
                    }
                } );
            }

            function getAttrBlockInnerHTML() {
                return `
                    <h2 class="aps-attr-heading">
                        ${aps.txt.select.replace( '{}', attributeName.toLowerCase() )}
                    </h2>
                    ${getDescriptionHTML()}
                    <div class="aps-terms-block" data-attr-slug="${attributeSlug}">
                        ${getTermsHTML()}
                    </div>
                    ${getPaginationHTML()}`;
            }

            const attrBlockSelector = `.aps-attr-block[data-attr-block="${attributeSlug}"]`;
            let attrBlock;

            if ( rerenderOnlyTarget ) {
                attrBlock = document.querySelector( attrBlockSelector );
                attrBlock.innerHTML = getAttrBlockInnerHTML()
            } else {
                apsPopupContent.insertAdjacentHTML( 'beforeend', `
                    ${maybeGetSeparator()}
                    <div class="aps-attr-block" data-attr-block="${attributeSlug}" data-attr-block-num="${i + 1}"
                    data-auto-select-the-only-option="${attributeData['auto_select_the_only_available_option']}">
                        ${getAttrBlockInnerHTML()}
                    </div>
                ` );
                attrBlock = document.querySelector( attrBlockSelector );
                attrBlock.style.setProperty(
                    `--aps-attr-columns-desktop`,
                    attributeData.number_of_columns_desktop
                );
                attrBlock.style.setProperty(
                    `--aps-attr-columns-tablet`,
                    attributeData.number_of_columns_tablet
                );
                attrBlock.style.setProperty(
                    `--aps-attr-columns-mobile`,
                    attributeData.number_of_columns_mobile
                );
            }

            const attrBlockNum = parseInt( attrBlock.getAttribute( 'data-attr-block-num' ) );

            if (
                aps.settings.selectAttributesStepByStep &&
                ! aps.settings.automaticallyShowTheNextAttributeModal &&
                renderAttributeSlug === attributeSlug &&
                select.value
            ) {
                addSelectNextAttributeBlock();
            }

            const options = attrBlock.querySelectorAll( '.aps-option' );
            options.forEach( option => {
                const termSlug = option.getAttribute( 'data-slug' );
                const termName = option.getAttribute( 'data-name' );

                // Tippy.js (tooltips).

                const tooltip = attributeData.terms[ termSlug ].tooltip;
                if ( tooltip ) {
                    tippy( option, {
                        content: tooltip,
                        theme: 'aps',
                        allowHTML: true,
                    } );
                }

                // Option select.

                option.addEventListener( 'click', function( event ) {
                    lastClickedAttrBlock = attrBlock;

                    if ( ! event.data || ! event.data.autoClick ) {
                        lastClickedAttrHeadingViewportTop = attrBlock.querySelector( '.aps-attr-heading' ).getBoundingClientRect().top;
                        lastManuallyClickedAttrBlock = attrBlock;
                    }

                    if (
                        ! attributeData.skip_select_confirmation &&
                        ! option.classList.contains( 'active' ) &&
                        1 !== termsMetaData.availableTermsCount // When only one available option, do not ask.
                    ) {
                        Swal.fire( {
                            title: aps.txt.confirmSelectionQuestion
                                .replace( '{$1}', attributeName.toLowerCase() ).replace( '{$2}', termName ),
                            icon: 'question',
                            showCancelButton: true,
                            confirmButtonColor: '#2271b1',
                            cancelButtonColor: '#d33',
                            confirmButtonText: aps.txt.yes,
                            cancelButtonText: aps.txt.no,
                            customClass: {
                                container: 'aps-swal-popup'
                            },
                            scrollbarPadding: false,
                        } ).then( (result) => {
                            if ( result.isConfirmed ) {
                                optionClick();
                            }
                        } );
                    } else {
                        optionClick();
                    }

                    function optionClick() {

                        // Select the option.

                        const toSelect = ! option.classList.contains( 'active' );

                        if (
                            ! aps.settings.deselectOptionOnTheClick ||
                            toSelect
                        ) {
                            selectOption( termSlug, attributeSlug, option.classList.contains( 'grayed-out' ) );

                            if (
                                aps.settings.automaticallyShowTheNextAttributeModal &&
                                ! aps.settings.showAllAttributesInOneModal
                            ) {
                                showTheNextAttributeModal( attributeSlug );
                                return;
                            }
                        } else {
                            selectOption( '', attributeSlug );
                        }

                        // Closes the popup or renders a new one.

                        if ( toSelect &&
                            (
                                (
                                    ! aps.settings.selectAttributesStepByStep &&
                                    (
                                        ! getUnselectedAttributesList().length ||
                                        ! aps.settings.showAllAttributesInOneModal
                                    )
                                ) ||
                                (
                                    aps.settings.automaticallyShowTheNextAttributeModal &&
                                    ! getUnselectedAttributesList().length
                                )
                            )
                        ) {
                            closePopup();
                        } else {
                            renderPopupContent( attributeSlug );
                        }
                    }
                } );
            } );

            const apsTermsBlock = attrBlock.querySelector( '.aps-terms-block' );
            const paginationContainer = attrBlock.querySelector( '.aps-pagination-container' );

            /**
             * When there is only one available option, auto-click it.
             */
            if (
                'true' === attrBlock.getAttribute( 'data-auto-select-the-only-option' ) &
                ! onProgressBarClick &&
                ! aps.settings.selectAttributesStepByStep &&
                aps.settings.automaticallyShowTheNextAttributeModal &&
                1 === termsMetaData.availableTermsCount &&
                ! termsMetaData.activeTermName && // No active term.
                (
                    ! aps.settings.showAllAttributesInOneModal ||
                    attributeSlug === getTheNextAttributeName( renderAttributeSlug ) // When "all in one modal", auto-click only for the next attribute (not for all).
                )
            ) {
                optionToClickOnRenderFinish = attrBlock.querySelector( `.aps-option[data-slug="${termsMetaData.availableTermsList[0]}"]` );
            }

            /**
             * Adds pagination click listeners.
             */
            ( function addPaginationClickListeners() {
                if ( paginationContainer ) {
                    const attrBlock = paginationContainer.closest( '.aps-attr-block' );

                    let paginationElements = paginationContainer.querySelectorAll( '.aps-pagination-element' );
                    for ( let paginationElement of paginationElements ) {
                        paginationElement.addEventListener( 'click', event => {
                            event.preventDefault();
                            let targetPage = parseInt( paginationElement.getAttribute( 'data-page' ) );
                            if ( pageNum !== targetPage ) {
                                renderPopupContent( attributeSlug, targetPage, true );
                            }
                        } );
                    }
                }
            } ) ();
        } );

        if ( optionToClickOnRenderFinish ) {
            let clickEvent = new Event( 'click' );
            clickEvent.data = { 'autoClick': true };
            return optionToClickOnRenderFinish.dispatchEvent( clickEvent );
        }

        /**
         * When "Show all attributes in one modal" is enabled, after click render, maybe scroll to:
         *
         * 1. The current attribute (to fix the scrolling bug);
         * 2. The next attribute.
         */
        if (
            aps.settings.showAllAttributesInOneModal
            && lastManuallyClickedAttrBlock
            && ! rerenderOnlyTarget // Do not scroll for pagination.
        ) {
            if ( ! lastClickedAttrBlock ) {
                lastClickedAttrBlock = lastManuallyClickedAttrBlock;
            }

            // Fix an issue when the popup rendering shows modal top.
            scrollToThePreviousPopupPosition( parseInt( lastManuallyClickedAttrBlock.getAttribute( 'data-attr-block-num' ) ) );

            /**
             * Scroll to the just shown select next attr button.
             */
            if ( 'object' === typeof selectNextAttrBtn ) { // Is Element.
                const isInViewport =
                    ( // Top of the button is in viewport.
                        selectNextAttrBtn.getBoundingClientRect().top > apsPopupContent.offsetTop &&
                        selectNextAttrBtn.getBoundingClientRect().top < apsPopupContent.offsetTop + apsPopupContent.clientHeight
                    ) &&
                    ( // Bottom of the button is in viewport.
                        selectNextAttrBtn.getBoundingClientRect().bottom > apsPopupContent.offsetTop &&
                        selectNextAttrBtn.getBoundingClientRect().bottom < apsPopupContent.offsetTop + apsPopupContent.clientHeight
                    );

                if ( ! isInViewport ) {
                    apsPopupContent.scrollTo( {
                        top: selectNextAttrBtn.offsetTop - apsPopupContent.clientHeight + selectNextAttrBtn.clientHeight + selectNextAttrBtn.clientHeight / 2,
                        behavior: 'smooth'
                    } );
                }
            }

            const lastClickedAttrBlockNum = parseInt( lastClickedAttrBlock.getAttribute( 'data-attr-block-num' ) );

            if (
                ! lastClickedAttrBlock.classList.contains( 'active' ) && // Option was activated.
                aps.settings.automaticallyShowTheNextAttributeModal &&
                ! aps.settings.selectAttributesStepByStep
            ) {
                scrollToTheNextAttributeBlock( lastClickedAttrBlockNum );
            }
        }
    }

    /**
     * Closes the popup.
     */
    function closePopup() {
        lastManuallyClickedAttrBlock = false; // Always open a new pop-up at the top.
        apsPopup.classList.remove( 'visible' );
    }

    // Select click.

    for ( const attribute in attributes ) {
        const attributeSelect = variationsTable.querySelector( `select#${attribute}` );
        if ( attributeSelect ) {
            /**
             * Detects left button mouse click.
             *
             * @param {Event} event
             *
             * @return {Boolean}
             */
            function wasLeftMouseBtnClicked( event ) {
                if ( event.metaKey || event.ctrlKey || event.altKey || event.shiftKey ) {
                    return false;
                } else if ( 'which' in event ) {
                    return event.which === 1;
                } else if ( 'buttons' in event ) {
                    return event.buttons === 1;
                } else {
                    return ( event.button === 1 || event.type === 'click' );
                }
            }

            attributeSelect.addEventListener( 'mousedown', e => {
                if ( ! wasLeftMouseBtnClicked( e ) ) {
                    return;
                }

                // Prevent the default dropdown to open.
                e.preventDefault();
                attributeSelect.blur();
                window.focus();

                // Open the popup.
                apsPopup.classList.add( 'visible' );
                renderPopupContent( attribute );
            } );
        }
    }

    // Close popup on click on dark area.

    apsPopup.addEventListener( 'click', event => {
        if ( apsPopup === event.target ) {
            closePopup();
        }
    } );

    // Close popup on Escape click.

    document.body.addEventListener( 'keydown', function( event ) {
        if ( event.key === 'Escape' ) {
            closePopup();
        }
    } );

    // Show all attributes in one modal.

    if ( ! aps.settings.showAllAttributesInOneModal ) {
        variationsTable.classList.add( 'aps-show' );
    } else {
        const attributesList = Object.keys( attributes );
        if ( attributesList.length ) {
            const openVariationsBtn = document.createElement( 'div' );
            openVariationsBtn.classList.add( 'aps-open-variations-button' );
            openVariationsBtn.innerHTML = `
                <span class="aps-open-text">${aps.txt.selectAttributes}</span>
                <span class="aps-open-arrow">&#10140;</span>
            `;
            openVariationsBtn.addEventListener( 'click', function() {
                const attributeSelect = document.querySelector( `select#${attributesList[0]}` );
                attributeSelect.dispatchEvent( new MouseEvent( 'mousedown' ) );
            } );
            variationsTable.parentNode.insertBefore( openVariationsBtn, variationsTable );
        }
    }

    // Show prices dynamically (based on the currently selected attributes).

    /**
     * Returns an array of available variations with attributes and prices.
     *
     * Empty string means "any value".
     *
     * Example:
     *
     * {
     *     "price": 15,
     *     "attributes": {
     *         "pa_color": "yellow",
     *         "pa_size": "medium",
     *         "pa_brand": "",
     *         "pa_ratio": ""
     *         "logo":     "Yes"
     *     }
     * }
     *
     * @return {Array}
     */
    function getAvailableFormattedVariations() {

        // Collects all variations in the neatly formatted array.

        const variations = JSON.parse( document.querySelector( '.variations_form' ).getAttribute( 'data-product_variations' ) );

        let formattedVariations = [];

        for ( const variation of variations ) {
            const price    = variation.display_price;
            let attributes = {};
            for ( const attributeSlug in variation.attributes ) {
                attributes[ attributeSlug.replace( /^attribute_/g, '' ) ] = variation.attributes[ attributeSlug ];
            }

            formattedVariations.push( {
                'price':      price,
                'attributes': attributes,
            } );
        }

        // Filters out all variations by throwing away the ones which can't be selected (for example, no variations for color blue when color red is selected).

        const selectedOptions = getSelectedOptions();

        const filteredVariations = [];

        for ( const variation of formattedVariations ) {
            let thisVariationCantBeSelected = false;

            for ( const attributeSlug in variation.attributes ) {
                const attributeValue = variation.attributes[ attributeSlug ];

                if (
                    attributeValue !== '' && // Not any.
                    attributeSlug in selectedOptions && selectedOptions[ attributeSlug ] !== attributeValue // Selected different value.
                ) {
                    thisVariationCantBeSelected = true;
                    break;
                }
            }

            if ( ! thisVariationCantBeSelected ) {
                filteredVariations.push( variation );
            }
        }

        return filteredVariations;
    }

    /**
     * Returns min and max price for the attribute and term.
     *
     * Considers the currently selected attributes.
     *
     * @param {String} attribute Attribute.
     * @param {String} term      Term.
     *
     * Example:
     *
     * {
     *     "min": 15,
     *     "max": 45
     * }
     *
     * @return {Object|Boolean} Attribute term prices, or false if attribute is not available.
     */
    function getAttributesTermPrices( attribute, term ) {
        const availableVariations = getAvailableFormattedVariations();

        let variationsThatMatchAttributeAndTerm = [];
        for ( const variation of availableVariations ) {
            if (
                variation.attributes.hasOwnProperty( attribute ) &&
                variation.attributes[ attribute ] === '' ||
                variation.attributes[ attribute ] === term
            ) {
                variationsThatMatchAttributeAndTerm.push( variation );
            }
        }

        if ( 0 === variationsThatMatchAttributeAndTerm.length ) {
            return false;
        }

        let prices = variationsThatMatchAttributeAndTerm.map(
            x => {
                return x.price;
            }
        );

        return {
            'min': Math.min( ...prices ),
            'max': Math.max( ...prices )
        }
    }
} );
