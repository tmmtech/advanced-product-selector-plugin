// noinspection JSUnusedLocalSymbols,JSUnresolvedVariable

/**
 * WC Edit Attributes (/wp-admin/edit.php?post_type=product&page=product_attributes) customization.
 *
 * @package Advanced-Product-Selector
 */
'use strict';


document.addEventListener( "DOMContentLoaded", function() {
    const aps = advancedProductSelector;

    document.querySelectorAll( '.row-actions' ).forEach( actionRow => {
        const aThatContainsSlug
            = actionRow.closest( 'td' ).querySelector( `table a[href^="edit-tags.php?taxonomy="]` );
        const attr = new URL( aThatContainsSlug.getAttribute( 'href' ), window.location.href )
            .searchParams.get( 'taxonomy' ).replace( /^pa_/g, '' );

        const wrap = document.createElement( 'div' );
        wrap.innerHTML = `<span class="edit"><a href="admin.php?page=aps&attr=${attr}">${aps.txt.productSelector}</a> | </span>`;

        const deleteAction = actionRow.querySelector( '.delete' );
        deleteAction.parentNode.insertBefore(
            wrap.querySelector( 'span' ),
            deleteAction
        );

        wrap.remove();
    } );
} );
