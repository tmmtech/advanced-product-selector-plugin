// noinspection JSUnusedLocalSymbols,JSUnresolvedVariable

/**
 * Contains screen-specified JS routines.
 *
 * @package Advanced-Product-Selector
 */
'use strict';


document.addEventListener( "DOMContentLoaded", function() {
    let aps = advancedProductSelector;

    const saveSettingsBtn = document.querySelector( '.aps-submit' );
    /**
     * "Save" click: Sends an AJAX with the menu settings to the server.
     */
    saveSettingsBtn.addEventListener( 'click', event => {
        event.preventDefault();

        const form = saveSettingsBtn.closest( 'form' );
        form.querySelectorAll( 'input[disabled]' ).forEach( input => { // Enable all disabled elements (to fetch FormData correctly).
            input.disabled = false;
        } );

        const formData = new FormData( form );

        // Sends AJAX with new settings.

        let data = {
            'action':      'advanced_product_selector_save_attr',
            'nonceToken':  aps.nonceToken,
            'attrID':      document.querySelector( 'form' ).getAttribute('data-attr-id'),
            'attrSlug':    document.querySelector( 'form' ).getAttribute('data-attr-slug'),
            'description': aps.getTinyMCEContent( 'attr-description' ),
            'numberOfColumnsDesktop':           formData.get( 'number_of_columns_desktop' ),
            'numberOfColumnsTablet':            formData.get( 'number_of_columns_tablet' ),
            'numberOfColumnsMobile':            formData.get( 'number_of_columns_mobile' ),
            'numPerPage':                       formData.get( 'num_per_page' ),
            'skipSelectConfirmation':           formData.get( 'skip_select_confirmation' ),
            'showPricePerAttribute':            formData.get( 'show_price_per_attribute' ),
            'attributeAdvancedSelector':        formData.get( 'attribute-advanced-selector' ),
            'textForNonAvailableOptions':       formData.get( 'text_for_non_available_options' ),
            'autoSelectTheOnlyAvailableOption': formData.get( 'auto_select_the_only_available_option' ),
        };

        // Saves attribute terms data.

        let termsOrder = [];
        let recommendedTerms = [];

        const termSettingRows = document.querySelectorAll( '.term-setting-row' );
        termSettingRows.forEach( row => {
            const termSlug = row.getAttribute('data-term-slug');

            // Attributes order.

            if ( ! slugsToResetOrderFor.includes( termSlug ) ) { // Saves all attributes except for those for whose order is reset on action apply.
                termsOrder.push( termSlug );
            }

            // Recommended.

            if ( 'on' === formData.get( `term-${termSlug}-recommended` ) ) {
                recommendedTerms.push( termSlug );
            }
        } );

        data['termsOrder']       = JSON.stringify( termsOrder );
        data['recommendedTerms'] = JSON.stringify( recommendedTerms );

        // Send and handle AJAX.

        let params = {
            method:      'POST',
            credentials: 'same-origin',
            headers:     new Headers( { 'Content-Type': 'application/x-www-form-urlencoded' } ),
            body:        new URLSearchParams( data )
        };

        fetch( ajaxurl, params ).then( response => {
            return response.json();
        } ).then( response => {
            if ( true === response.success ) {
                location.reload();
            } else {
                Swal.fire(
                    aps.txt.error,
                    response.data,
                    'error'
                );
            }

            location.reload();
        } );
    } );

    /**
     * The list of slugs for which the order will not be recorded.
     *
     * @type {String[]}
     */
    let slugsToResetOrderFor = [];

    // Bulk actions.

    const bulkActionsSubmitBtn = document.querySelector( '.bulkactions input[type="submit"]' );
    bulkActionsSubmitBtn.addEventListener( 'click', function() {
        const formData = new FormData( document.querySelector( 'form' ) );
        const action = formData.get( 'action' );

        if ( '-1' === action ) {
            return;
        }

        let selected = [];

        const termSettingRows = document.querySelectorAll( '.term-setting-row' );
        termSettingRows.forEach( row => {
            const attributeSlug = row.getAttribute('data-term-slug');

            if ( 'on' === formData.get( `term-${attributeSlug}-selected` ) ) {
                selected.push( attributeSlug );
            }
        } );

        if ( 'reset-order' === action ) {
            slugsToResetOrderFor = selected;
            selected.forEach( x => {
                saveSettingsBtn.click();
            } );
        }
    } );

    /**
     * SortableJS.
     */
    new Sortable(
        document.querySelector( 'tbody.aps-settings' ),
        { handle: '.aps-reorder-handle' }
    );
} );
