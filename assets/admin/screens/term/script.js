// noinspection JSUnusedLocalSymbols,JSUnresolvedVariable

/**
 * Contains screen-specified JS routines.
 *
 * @package Advanced-Product-Selector
 */
'use strict';


document.addEventListener( "DOMContentLoaded", function() {
    let aps = advancedProductSelector;

    /**
     * Primary data for the save.
     */
    let primarySaveData = {};

    // Color select.

    const colorSelects = document.querySelectorAll( '.aps-color-select' );
    colorSelects.forEach( colorSelect => {

        primarySaveData[ colorSelect.getAttribute( 'data-data-name' ) ] = colorSelect.getAttribute( 'data-color' );

        colorSelect.getAttribute( 'data-color' )

        const pickr = Pickr.create( {
            el: colorSelect,
            theme: 'classic',
            default: colorSelect.getAttribute( 'data-color' ),

            swatches: [
                'rgba(244, 67, 54, 1)',
                'rgba(233, 30, 99, 1)',
                'rgba(156, 39, 176, 1)',
                'rgba(103, 58, 183, 1)',
                'rgba(63, 81, 181, 1)',
                'rgba(33, 150, 243, 1)',
                'rgba(3, 169, 244, 1)',
                'rgba(0, 188, 212, 1)',
                'rgba(0, 150, 136, 1)',
                'rgba(76, 175, 80, 1)',
                'rgba(139, 195, 74, 1)',
                'rgba(205, 220, 57, 1)',
                'rgba(255, 235, 59, 1)',
                'rgba(255, 193, 7, 1)'
            ],

            components: {

                // Main components
                preview: true,
                opacity: true,
                hue: true,

                // Input / output Options
                interaction: {
                    hex: true,
                    rgba: true,
                    hsla: false,
                    hsva: false,
                    cmyk: false,
                    input: true,
                    clear: false,
                    save: true
                }
            }
        } );

        pickr.on( 'save', function( color ) {
            pickr.hide();

            primarySaveData[ colorSelect.getAttribute( 'data-data-name' ) ] = color.toRGBA().toString( 0 );
        } );
    } );

    const saveSettingsBtn = document.querySelector( '.aps-submit' );
    /**
     * "Save" click: Sends an AJAX with the menu settings to the server.
     */
    saveSettingsBtn.addEventListener( 'click', event => {
        event.preventDefault();

        // Sends AJAX with new settings.

        let data = Object.assign( primarySaveData, {
            'action':          'advanced_product_selector_save_term',
            'nonceToken':      aps.nonceToken,
            'termID':          document.querySelector( 'form' ).getAttribute('data-term-id'),
            'taxonomy':        document.querySelector( 'form' ).getAttribute('data-taxonomy'),
            'description':     aps.getTinyMCEContent( 'term-description' ),
            'tooltip':         aps.getTinyMCEContent( 'term-tooltip' ),
            'recommendedText': aps.getTinyMCEContent( 'term-recommended-text' ),
        } );

        const img = document.querySelector( '.aps-image-container img' );
        if ( img ) {
            data['image'] = img.getAttribute( 'data-attachment-id' );
        }

        const imgHover = document.querySelector( '.aps-image-hover-container img' );
        if ( imgHover ) {
            data['imageHover'] = imgHover.getAttribute( 'data-attachment-id' );
        }

        // Send and handle AJAX.

        let params = {
            method:      'POST',
            credentials: 'same-origin',
            headers:     new Headers( { 'Content-Type': 'application/x-www-form-urlencoded' } ),
            body:        new URLSearchParams( data )
        };

        fetch( ajaxurl, params ).then( response => {
            return response.json();
        } ).then( response => {
            if ( true === response.success ) {
                location.reload();
            } else {
                Swal.fire(
                    aps.txt.error,
                    response.data,
                    'error'
                );
            }

            location.reload();
        } );
    } );
} );
