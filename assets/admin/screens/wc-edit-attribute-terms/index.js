// noinspection JSUnusedLocalSymbols,JSUnresolvedVariable

/**
 * Assets WC Edit Attribute Terms screen (/wp-admin/edit-tags.php?taxonomy=pa_brand&post_type=product) customization.
 *
 * @package Advanced-Product-Selector
 */
'use strict';


document.addEventListener( "DOMContentLoaded", function() {
    const aps = advancedProductSelector;

    document.querySelectorAll( '.row-actions' ).forEach( actionRow => {
        const aHref
            = actionRow.closest( 'td' ).querySelector( `a[href*="edit-tags.php"]` );
        const urlParams = new URL( aHref.getAttribute( 'href' ), window.location.href ).searchParams;
        const attr   = urlParams.get( 'taxonomy' ).replace( /^pa_/g, '' );
        const termID = urlParams.get( 'tag_ID' );
        const termSlug = aps.termsIds[ termID ];

        const wrap = document.createElement( 'div' );
        wrap.innerHTML = `<span class="edit"><a href="admin.php?page=aps&attr=${attr}&term=${termSlug}">
            ${aps.txt.productSelector}</a> | </span>`;

        const deleteAction = actionRow.querySelector( '.delete' );
        deleteAction.parentNode.insertBefore(
            wrap.querySelector( 'span' ),
            deleteAction
        );

        wrap.remove();
    } );
} );
