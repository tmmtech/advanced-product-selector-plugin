// noinspection JSUnusedLocalSymbols,JSUnresolvedVariable

/**
 * Contains screen-specified JS routines.
 *
 * @package Advanced-Product-Selector
 */
'use strict';


document.addEventListener( "DOMContentLoaded", function() {
    let aps = advancedProductSelector;

    const saveSettingsBtn = document.querySelector( '.aps-submit' );
    /**
     * "Save" click: Sends an AJAX with the menu settings to the server.
     */
    saveSettingsBtn.addEventListener( 'click', event => {
        event.preventDefault();

        const form = saveSettingsBtn.closest( 'form' );
        form.querySelectorAll( 'input[disabled]' ).forEach( input => { // Enable all disabled elements (to fetch FormData correctly).
            input.disabled = false;
        } );

        const formData = new FormData( form );

        // Sends AJAX with new settings.

        let data = {
            'action':                                     'advanced_product_selector_save',
            'nonceToken':                                 aps.nonceToken,
            'useSpecifiedOrder':                          formData.get( 'use-specified-order' ),
            'showAllAttributesInOneModal':                formData.get( 'show-all-attributes-in-one-modal' ),
            'selectAttributesStepByStep':                 formData.get( 'select-attributes-step-by-step' ),
            'attributesStepByStepSelectionText':          formData.get( 'attributes-step-by-step-selection-text' ),
            'automaticallyShowTheNextAttributeModal':     formData.get( 'automatically-show-the-next-attribute-modal' ),
            'howToTreatUnavailableAttributeCombinations': formData.get( 'how-to-treat-unavailable-attribute-combinations' ),
            'hideNotConfiguredTerms':                     formData.get( 'hide-not-configured-terms' ),
            'deselectOptionOnTheClick':                   formData.get( 'deselect-option-on-the-click' ),
            'nonAvailableOptionsTextPlacement':           formData.get( 'non-available-options-text-placement' ),
        };

        // Saves attributes data.

        let attributesOrder = [];
        let advancedSelectorAttributes = [];

        const attributeSettingRows = document.querySelectorAll('.attribute-setting-row');
        attributeSettingRows.forEach( row => {
            const attributeSlug = row.getAttribute('data-attribute-slug');

            // Attributes order.

            if ( ! slugsToResetOrderFor.includes( attributeSlug ) ) { // Saves all attributes except for those for whose order is reset on action apply.
                attributesOrder.push( attributeSlug );
            }

            // Advanced selector attributes.

            if ( 'on' === formData.get( `attribute-${attributeSlug}-advanced-selector` ) ) {
                advancedSelectorAttributes.push( attributeSlug );
            }
        } );

        data['attributesOrder']  = JSON.stringify( attributesOrder );
        data['advancedSelectorAttributes']  = JSON.stringify( advancedSelectorAttributes );

        // Custom arrow image.

        const arrowImg = document.querySelector( '.aps-arrow-image-container img' );
        if ( arrowImg ) {
            data['arrowImage'] = arrowImg.getAttribute( 'data-attachment-id' );
        }

        // Send and handle AJAX.

        let params = {
            method:      'POST',
            credentials: 'same-origin',
            headers:     new Headers( { 'Content-Type': 'application/x-www-form-urlencoded' } ),
            body:        new URLSearchParams( data )
        };

        fetch( ajaxurl, params ).then( response => {
            return response.json();
        } ).then( response => {
            if ( true === response.success ) {
                location.reload();
            } else {
                Swal.fire(
                    aps.txt.error,
                    response.data,
                    'error'
                );
            }

            location.reload();
        } );
    } );

    /**
     * The list of slugs for which the order will not be recorded.
     *
     * @type {String[]}
     */
    let slugsToResetOrderFor = [];

    // Bulk actions.

    const bulkActionsSubmitBtn = document.querySelector( '.bulkactions input[type="submit"]' );
    bulkActionsSubmitBtn.addEventListener( 'click', function() {
        const formData = new FormData( document.querySelector( 'form' ) );
        const action = formData.get( 'action' );

        if ( '-1' === action ) {
            return;
        }

        let selected = [];

        const attributeSettingRows = document.querySelectorAll( '.attribute-setting-row' );
        attributeSettingRows.forEach( row => {
            const attributeSlug = row.getAttribute('data-attribute-slug');

            if ( 'on' === formData.get( `attribute-${attributeSlug}-selected` ) ) {
                selected.push( attributeSlug );
            }
        } );

        if ( 'enable-advanced-select' === action ) {
            selected.forEach( x => {
                document.querySelector( `input[name="attribute-${x}-advanced-selector"]` ).checked = true;
            } );
        } else if ( 'disable-advanced=select' === action ) {
            selected.forEach( x => {
                document.querySelector( `input[name="attribute-${x}-advanced-selector"]` ).checked = false;
            } );
        } else if ( 'reset-order' === action ) {
            slugsToResetOrderFor = selected;
            selected.forEach( x => {
                saveSettingsBtn.click();
            } );
        }
    } );

    /**
     * SortableJS.
     */
    new Sortable(
        document.querySelector( 'tbody.aps-settings' ),
        { handle: '.aps-reorder-handle' }
    );

    // Settings Export / Import.

    /**
     * Settings export.
     */
    let exportSettingsLink = document.querySelector( '.aps-export-settings-link' );
    exportSettingsLink.addEventListener( 'click', () => {
        Swal.fire( {
            title: aps.txt.exportPopupTitle,
            text: aps.txt.exportPopupText,
            icon: 'info',
            showCancelButton: true,
            confirmButtonColor: '#2271b1',
            cancelButtonColor: '#d33',
            confirmButtonText: aps.txt.yes,
            cancelButtonText: aps.txt.no
        } ).then( (result) => {
            let bundleImages = result.isConfirmed ? '1' : '0';
            window.location.href = `${window.location.href}&nonce=${aps.nonceToken}&aps-action=export-settings&bundleImages=${bundleImages}`;
        } );
    } );

    /**
     * Settings import.
     */
    let importSettingsLink      = document.querySelector( '.aps-import-settings-link' ),
        importSettingsFileInput = document.querySelector( 'input[type="file"][name="aps-import-settings-link-file-input"]' );
    importSettingsLink.addEventListener( 'click', () => {
        importSettingsFileInput.click();
    } );

    importSettingsFileInput.addEventListener( 'change', () => {
        let file   = importSettingsFileInput.files[0],
            type   = file.type,
            reader = new FileReader();

        reader.onload = function() {
            let data = new URLSearchParams( {
                'action':     'advanced_product_selector_import_settings',
                'nonceToken':  aps.nonceToken,
                'type':        type,
                'file':        reader.result,
            } ), params = {
                method:      'POST',
                credentials: 'same-origin',
                headers:     new Headers( { 'Content-Type': 'application/x-www-form-urlencoded' } ),
                body:        data
            };

            fetch( ajaxurl, params ).then( response => {
                return response.json();
            } ).then( response => {
                location.reload();
            } );
        };
        reader.readAsDataURL( file );

        // Show an info popup.

        Swal.fire( {
            title: aps.txt.settingsAreImporting,
            text: aps.txt.pleaseWait,
            icon: 'info',
        } );
    } );

    // Reset Settings.

    let resetSettingsLink = document.querySelector( '.aps-reset-settings-link' );
    resetSettingsLink.addEventListener( 'click', () => {

        Swal.fire( {
            title: aps.txt.resetAllSettingsNotice,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#2271b1',
            cancelButtonColor: '#d33',
            confirmButtonText: aps.txt.yesResetAllSettings,
            cancelButtonText: aps.txt.no,
        } ).then( (result) => {
            if ( result.isConfirmed ) {
                let data = new URLSearchParams( {
                    'action':     'advanced_product_selector_reset_settings',
                    'nonceToken': aps.nonceToken,
                } ), params = {
                    method:      'POST',
                    credentials: 'same-origin',
                    headers:     new Headers( { 'Content-Type': 'application/x-www-form-urlencoded' } ),
                    body:        data
                };

                fetch( ajaxurl, params ).then( response => {
                    return response.json();
                } ).then( response => {
                    location.reload();
                } );
            }
        } );
    } );
} );
