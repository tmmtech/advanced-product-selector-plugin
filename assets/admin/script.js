// noinspection JSUnusedLocalSymbols,JSUnresolvedVariable

/**
 * Admin common JS routines (used on more than 1 screen)..
 *
 * @package Advanced-Product-Selector
 */
'use strict';

document.addEventListener( "DOMContentLoaded", function() {
    let aps = advancedProductSelector;

    window.advancedProductSelector.getTinyMCEContent = function( TMCEditorID ) {
        return document.getElementById( `wp-${TMCEditorID}-wrap` ).classList.contains( 'tmce-active' ) ?
            tinyMCE.get( TMCEditorID ).getContent() : document.querySelector( `#${TMCEditorID}` ).value;
    }

    /**
     * Don't submit the form.
     */
    document.querySelector( 'form' ).addEventListener( 'submit', event => {
        event.preventDefault();
    } );

    /*
     * Allow only digits for number inputs.
     *
     * 0 for null values
     * 8 for backspace
     * 48-57 for 0-9 numbers
     * 45 for minus sign
     */
    let numberInputs = document.querySelectorAll( '.aps-number-input' );
    for ( let numberInput of numberInputs ) {
        numberInput.addEventListener( 'keypress', function( event ) {
            if ( event.which !== 8 && event.which !== 0 && event.which !== 109 && event.which < 45 || event.which > 57 ) {
                event.preventDefault();
            }
        } );
    }

    // Makes "fake disabled" checkboxes clickable.
    // Note: Fake disabled checkboxes are needed to make "Select all" checkbox do not select them, and on Shift select also.
    // So it's just the circumventing of the default checkboxes script.

    const fakeDisabledCheckboxes = document.querySelectorAll('input[type="checkbox"]:disabled.fake-disabled');
    fakeDisabledCheckboxes.forEach( checkbox => {
        const elem = document.createElement('div');
        elem.classList.add( 'elem-that-covers-fake-disabled-checkboxes-and-gets-events' );
        elem.setAttribute( 'tabindex', '0' );
        elem.addEventListener( 'click', function() {
            checkbox.checked = ! checkbox.checked;
        } );
        elem.addEventListener( 'keydown', function( e ) {
            if( 'Space' === e.code ) {
                elem.click();
                e.preventDefault();
            }
        } );
        checkbox.parentNode.insertAdjacentElement( 'afterbegin', elem );
    } );

    const noImageHTMLs = {
        'noCustomArrowForProgressBar': `<span class="p-like mt-5 mb-5">${aps.txt.noCustomArrowForProgressBarSet}</span>`,
    };

    const imageUploadButtons = document.querySelectorAll( '.aps-image-upload' );
    imageUploadButtons.forEach( imageUploadBtn => {
        imageUploadBtn.addEventListener( 'click', function() {
            // Inits wp.media.

            // Extends the wp.media object.
            let mediaUploader = wp.media.frames.file_frame = wp.media( {
                multiple: false
            } );

            // When a file is selected.
            mediaUploader.on( 'select', function() {
                const media = mediaUploader.state( '' ).get('selection').first().toJSON();
                const imgContainer = imageUploadBtn.closest( '.aps-image-block' ).querySelector( '.aps-image-container' );
                imgContainer.innerHTML = `<img src="${media.url}" alt="${media.alt}" data-attachment-id="${media.id}">`;
            } );

            // Opens the uploader dialog.
            mediaUploader.open();
        } );
    } );

    const imageUnsetButtons = document.querySelectorAll( '.aps-image-unset' );
    imageUnsetButtons.forEach( imageUnsetButton => {
        imageUnsetButton.addEventListener( 'click', function() {
            const imgContainer = imageUnsetButton.closest( '.aps-image-block' ).querySelector( '.aps-image-container' );
            imgContainer.innerHTML = imgContainer.hasAttribute( 'data-no-image-property' )
                ? noImageHTMLs[ imgContainer.getAttribute( 'data-no-image-property' ) ] : aps.txt.noImageAddedYet;
        } );
    } )
} );
