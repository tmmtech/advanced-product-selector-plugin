<?php
/**
 * Defines plugin options.
 *
 * Format:
 *  'default':  Default option value.           If not set, equals false.
 *  'autoload': Whether to autoload the option. If not set, equals true.
 *  'type':     Option type. Optional. Can be: "image_id"
 *
 * @package Advanced-Product-Selector
 */

return [

    /*
     * Whether to allow specified order to have effect for all products.
     */
    'aps-setting-use-specified-order'                    => [
        'default' => '',
    ],

    /*
     * Attributes order.
     */
    'aps-setting-attributes-order'                       => [
        'default' => [],
    ],

    /*
     * Attributes for which advanced selector is enabled.
     */
    'aps-setting-advanced-selector-attributes'           => [
        'default' => [],
    ],

    /*
     * Show all attributes stacked in the same modal.
     */
    'aps-setting-show-all-attributes-in-one-modal'       => [
        'default' => '',
    ],

    /*
     * Select attributes step by step.
     */
    'aps-setting-select-attributes-step-by-step'         => [
        'default' => '1',
    ],

    /*
     * Text to show when the visitor needs to select attributes step by step.
     */
    'aps-setting-attributes-step-by-step-selection-text' => [
        'default' => '',
    ],

    /*
     * When the user selects an attribute automatically show the next attribute modal (wizard style).
     */
    'aps-setting-automatically-show-the-next-attribute-modal' => [
        'default' => '',
    ],

    /*
     * How to treat unavailable attribute combinations.
     */
    'aps-setting-how-to-treat-unavailable-attribute-combinations' => [
        'default' => 'hide',
    ],

    /*
     * Hide not configured terms.
     */
    'aps-setting-hide-not-configured-terms'              => [
        'default' => '',
    ],

    /*
     * Custom progress bar arrow.
     */
    'aps-setting-custom-progress-bar-arrow'              => [
        'default' => '',
        'type'    => 'image_id',
    ],

    /*
     * Whether to deselect option on the click.
     */
    'aps-setting-deselect-option-on-the-click'           => [
        'default' => '',
    ],

    /*
     * Attributes without variations
     */
    'aps-attributes-without-variations'                  => [
        'default' => [],
    ],

    /*
     * Placement of the text for non-available options.
     */
    'aps-setting-non-available-options-text-placement'   => [
        'default' => 'over-the-image',
    ],
];
