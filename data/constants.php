<?php
/**
 * Defines plugin constants.
 *
 * @package Advanced-Product-Selector
 */

$plugin_file = realpath( __DIR__ . '/../advanced-product-selector.php' );

/*
 * The URL to the plugin.
 */
define( 'ADVANCED_PRODUCT_SELECTOR_URL', plugin_dir_url( $plugin_file ) );

/*
 * The filesystem directory path to the plugin.
 */
define( 'ADVANCED_PRODUCT_SELECTOR_DIR', plugin_dir_path( $plugin_file ) );

/*
 * The version of the plugin.
 */
define( 'ADVANCED_PRODUCT_SELECTOR_VERSION', get_file_data( $plugin_file, [ 'Version' ] )[0] );

/*
 * The filename of the plugin including the path.
 */
define( 'ADVANCED_PRODUCT_SELECTOR_FILE', $plugin_file );

/*
 * Plugin basename.
 */
define( 'ADVANCED_PRODUCT_SELECTOR_BASENAME', plugin_basename( ADVANCED_PRODUCT_SELECTOR_FILE ) );
