<?php
/**
 * Advanced_Product_Selector Uninstall
 *
 * Deletes Advanced_Product_Selector options and other data.
 *
 * @package Advanced-Product-Selector
 * @since 0.0.1
 */

// Security check.

if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
    exit;
}

require_once __DIR__ . '/data/constants.php';

// Deletes options.

$all_options      = require __DIR__ . '/data/options.php';
$settings_options = [];

foreach ( $all_options as $option => $option_data ) {
    delete_option( $option );
}
