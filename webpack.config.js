require( 'dotenv' ).config({ path: './node.env', override: true });

const defaultConfig = require("@wordpress/scripts/config/webpack.config");
const path = require("path");

module.exports = {
  ...defaultConfig,
  entry: {
    'admin/index':        './assets/admin/index.js',
    'public/index':       './assets/public/index.js',
    'admin/screens/attr': './assets/admin/screens/attr/index.js',
    'admin/screens/main': './assets/admin/screens/main/index.js',
    'admin/screens/term': './assets/admin/screens/term/index.js',
    'admin/screens/wc-edit-attribute-terms': './assets/admin/screens/wc-edit-attribute-terms/index.js',
    'admin/screens/wc-edit-attributes':      './assets/admin/screens/wc-edit-attributes/index.js',
  },
  output: {
    ...defaultConfig.output,
    path: path.resolve( __dirname, 'assets-build' ),
  }
};
