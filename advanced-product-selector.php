<?php
/**
 * Plugin Name: Advanced Product Selector
 * Description: A plugin that extends the default WooCommerce selector for variable products.
 * Version:     0.1.18
 * Text Domain: aps
 * Author:      TMM Technology
 * Author URI:  https://tmm.ventures/
 * Requires PHP: 7.4
 * License:     GPLv3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.html
 *
 * @package Advanced-Product-Selector
 */

namespace Advanced_Product_Selector;

/**
 * Main Advanced_Product_Selector class.
 */
final class Advanced_Product_Selector {

    /**
     * File System instance.
     *
     * @var \WP_Filesystem_Base
     */
    public static $fs;

    /**
     * AJAX class.
     *
     * @var AJAX
     */
    public static $ajax;

    /**
     * Options.
     *
     * @var Options
     */
    public static $options;

    /**
     * Attributes order.
     *
     * @var Reorder
     */
    public static $attributes_order;

    /**
     * Plugin slug.
     *
     * @var string
     */
    public static $slug;

    /**
     * Plugin version.
     *
     * @var string
     */
    public static $version;

    /**
     * Plugin name.
     *
     * @var string
     */
    public static $name;

    /**
     * Constructor.
     */
    public function __construct() {
        $this->define_constants();
        $this->import_plugin_files();
        $this->init_filesystem_class();

        self::$fs = $GLOBALS['wp_filesystem'];

        add_action(
            'plugins_loaded',
            function() {
                new \WP_Plugins_Core\WP_Plugins_Core( $this );

                // Load translations.
                $this->load_plugin_textdomain();

                // Options.
                self::$options = new Options();

                // Enabled WC is a dependency (requirement) to start the plugin.
                WC_Dependency::add();

                // Assets.
                Assets\Assets::init();

                // WooCommerce is active.
                if ( WC_Dependency::is_wc_installed_and_active() ) {
                    // Adds plugin settings links to plugins admin screen.
                    add_filter( 'plugin_action_links_' . ADVANCED_PRODUCT_SELECTOR_BASENAME, [ $this, 'plugin_action_links' ] );

                    add_action(
                        'init',
                        function () {
                            // Menu.
                            new Admin_Menu();

                            // AJAX Handler.
                            self::$ajax = new AJAX();

                            // Attributes and terms reorder.
                            self::$attributes_order = new Reorder();
                        }
                    );
                }
            }
        );
    }

    /**
     * Defines constants.
     */
    private function define_constants() {
        require_once __DIR__ . '/data/constants.php';

        /**
         * Plugin name.
         */
        self::$name = get_file_data( ADVANCED_PRODUCT_SELECTOR_FILE, [ 'Plugin Name' ] )[0];

        /**
         * Plugin slug.
         */
        $dir_parts  = explode( '/', ADVANCED_PRODUCT_SELECTOR_DIR );
        self::$slug = $dir_parts[ array_search( 'plugins', $dir_parts, true ) + 1 ];

        /**
         * Plugin version.
         */
        self::$version = ADVANCED_PRODUCT_SELECTOR_VERSION;
    }

    /**
     * Imports plugin files.
     */
    private function import_plugin_files() {
        $src_files = [
            'assets/screens/class-assets-main-screen',
            'assets/screens/class-assets-attr-screen',
            'assets/screens/class-assets-term-screen',
            'assets/screens/class-assets-wc-edit-attributes',
            'assets/screens/class-assets-wc-edit-attribute-terms',
            'assets/class-public-assets',
            'assets/class-assets',
            'class-attributes-prices',
            'class-admin-menu',
            'class-ajax',
            'class-options',
            'class-settings-export',
            'class-settings-import',
            'class-wc-dependency',
            'class-reorder',
            'class-data',
            'class-html',
            'class-attributes-without-variations',
        ];
        foreach ( $src_files as $file ) {
            require_once ADVANCED_PRODUCT_SELECTOR_DIR . 'src/' . $file . '.php';
        }

        $files = [
            'vendor/tmmtech/wp-plugins-core/wp-plugins-core',
            'vendor/autoload_packages',
        ];
        foreach ( $files as $file ) {
            require_once ADVANCED_PRODUCT_SELECTOR_DIR . $file . '.php';
        }
    }

    /**
     * Loads textdomain.
     */
    private function load_plugin_textdomain() {
        load_plugin_textdomain(
            'aps',
            false,
            dirname( ADVANCED_PRODUCT_SELECTOR_BASENAME ) . '/languages'
        );
    }

    /**
     * Inits filesystem class, which has to be declared in $wp_filesystem global variable.
     */
    private function init_filesystem_class() {
        if ( ! function_exists( 'WP_Filesystem' ) ) {
            require_once ABSPATH . 'wp-admin/includes/file.php';
        }

        if ( ! class_exists( 'WP_Filesystem_Base' ) ) {
            WP_Filesystem();
        }
    }

    /**
     * Show settings link on the plugin screen.
     *
     * @param mixed $links Plugin Action links.
     *
     * @return array
     */
    public function plugin_action_links( $links ) {
        $action_links = array(
            'settings' => '<a href="' . admin_url( 'admin.php?page=aps' ) . '" aria-label="' .
                esc_attr__( 'View Advanced Product Selector settings', 'aps' ) . '">' .
                esc_html__( 'Settings', 'aps' ) . '</a>',
        );

        return array_merge( $action_links, $links );
    }
}
new Advanced_Product_Selector();
