<?php
/**
 * A class to render HTML.
 *
 * @package Advanced-Product-Selector
 * @since   0.0.1
 */

namespace Advanced_Product_Selector;

/**
 * HTML class.
 */
final class HTML {

    /**
     * Returns "img" tag.
     *
     * @param int    $attachment_id
     * @param bool   $add_attachment_id Whether to add attachment ID.
     * @param string $image_class       The class to add to the image.
     *
     * @return string
     */
    public static function get_img( $attachment_id, $add_attachment_id = false, $image_class = '' ) {
        if ( ! $attachment_id ) {
            return '';
        }
        $image_data = Data::get_image_data( $attachment_id );
        if ( ! $image_data ) {
            return '';
        }
        $attachment_id_attr = $add_attachment_id ? "data-attachment-id='$attachment_id'" : '';
        $image_class        = $image_class ? "class=\"$image_class\"" : '';
        $image_html         = "<img $image_class src=\"{$image_data['src']}\" alt=\"{$image_data['alt']}\" $attachment_id_attr>";
        return wp_image_add_srcset_and_sizes( $image_html, wp_get_attachment_metadata( $attachment_id ), $attachment_id );
    }
}
