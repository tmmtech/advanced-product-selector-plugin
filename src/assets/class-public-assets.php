<?php
/**
 * Assets
 *
 * Loads public assets (JS, CSS), adds data for them.
 *
 * @package Advanced-Product-Selector
 * @since   0.0.1
 */

namespace Advanced_Product_Selector\Assets;

use Advanced_Product_Selector\Advanced_Product_Selector;
use Advanced_Product_Selector\Data;
use Advanced_Product_Selector\HTML;
use Advanced_Product_Selector\Attributes_Prices;
use Exception;

/**
 * Public assets class.
 */
final class Publ {

    /**
     * Inits.
     */
    public static function init() {
        global $product;

        if ( $product && ! is_object( $product ) ) {
            $product = wc_get_product( get_the_ID() );
        }

        if ( $product && $product->is_type( 'variable' ) ) {
            // Load public assets only for variable products.
            $class = __CLASS__;
            new $class();
        }
    }

    /**
     * Constructor.
     *
     * @throws Exception Exception.
     */
    public function __construct() {
        $this->register_libs();
        $this->styles();
        $this->scripts();
        $this->data_to_scripts();
    }

    /**
     * Registers libs.
     */
    private function register_libs() {

        // Popper + Tippy.js.

        wp_register_script(
            'aps-lib-popper',
            ADVANCED_PRODUCT_SELECTOR_URL . 'libs/@popperjs/core/dist/umd/popper.min.js',
            [],
            '2.11.5',
            true
        );
        wp_register_script(
            'aps-lib-tippyjs',
            ADVANCED_PRODUCT_SELECTOR_URL . 'libs/tippy.js/dist/tippy-bundle.umd.min.js',
            [ 'aps-lib-popper' ],
            '6.3.7',
            true
        );
    }

    /**
     * Loads styles.
     */
    private function styles() {
        wp_enqueue_style(
            'aps-public-style',
            ADVANCED_PRODUCT_SELECTOR_URL . 'assets-build/public/index.css',
            [ 'tmm-wp-plugins-core-lib-sweetalert2' ],
            ADVANCED_PRODUCT_SELECTOR_VERSION
        );
    }

    /**
     * Loads scripts.
     */
    private function scripts() {
        wp_enqueue_script(
            'aps-public-script',
            ADVANCED_PRODUCT_SELECTOR_URL . 'assets-build/public/index.js',
            [ 'tmm-wp-plugins-core-lib-sweetalert2', 'aps-lib-tippyjs', 'jquery' ],
            ADVANCED_PRODUCT_SELECTOR_VERSION,
            true
        );
    }

    /**
     * Loads data to scripts.
     *
     * @throws Exception Exception.
     */
    private function data_to_scripts() {
        global $product;

        $page_embed_data = [];

        $product_attributes = $product->get_attributes();

        $advanced_selector_attributes = array_map(
            function ( $x ) {
                return 'pa_' . $x;
            },
            Advanced_Product_Selector::$options->get( 'aps-setting-advanced-selector-attributes' )
        );

        $hide_not_configured_terms = Advanced_Product_Selector::$options->get( 'aps-setting-hide-not-configured-terms' ) === '1';
        $attributes_out_data       = [];
        foreach ( $product_attributes as $attribute_name => $attribute_data ) {
            $attribute_data = $attribute_data['data'];
            if (
                'pa_' === substr( $attribute_name, 0, 3 ) &&
                in_array( $attribute_name, $advanced_selector_attributes, true )
            ) {
                $terms_out_data = [];
                foreach ( $attribute_data['options'] as $term_id ) {
                    $term_data  = Data::get_term_data( $term_id );
                    $term_image = HTML::get_img( $term_data['image'] );
                    if ( ! $hide_not_configured_terms ||
                        $term_image // Adds only a term which is configured (has an image).
                    ) {
                        $term_data['image']                           = $term_image;
                        $term_data['hoverImage']                      = HTML::get_img( $term_data['image-hover'] );
                        $terms_out_data[ get_term( $term_id )->slug ] = $term_data;
                    }
                }
                if ( count( $terms_out_data ) ) { // Add an attribute only when it has terms.
                    $attributes_out_data[ $attribute_name ] = Data::get_attribute_data( $attribute_data['id'] );

                    $attributes_out_data[ $attribute_name ]['terms'] = Advanced_Product_Selector::$attributes_order->reorder_terms(
                        $terms_out_data,
                        $attributes_out_data[ $attribute_name ]['terms_order']
                    );
                    unset( $attributes_out_data[ $attribute_name ]['terms_order'] );
                }
            }
        }

        $page_embed_data['attributes'] = $attributes_out_data;
        $page_embed_data['settings']   = [
            'showAllAttributesInOneModal'                => Advanced_Product_Selector::$options->get( 'aps-setting-show-all-attributes-in-one-modal' ) === '1',
            'selectAttributesStepByStep'                 => Advanced_Product_Selector::$options->get( 'aps-setting-select-attributes-step-by-step' ) === '1',
            'automaticallyShowTheNextAttributeModal'     => Advanced_Product_Selector::$options->get( 'aps-setting-automatically-show-the-next-attribute-modal' ) === '1',
            'attributesStepByStepSelectionText'          => Advanced_Product_Selector::$options->get( 'aps-setting-attributes-step-by-step-selection-text' ),
            'howToTreatUnavailableAttributeCombinations' => Advanced_Product_Selector::$options->get( 'aps-setting-how-to-treat-unavailable-attribute-combinations' ),
            'deselectOptionOnTheClick'                   => Advanced_Product_Selector::$options->get( 'aps-setting-deselect-option-on-the-click' ),
            'customProgressBarArrow'                     => HTML::get_img(
                Advanced_Product_Selector::$options->get( 'aps-setting-custom-progress-bar-arrow' ),
                false,
                'aps-arrow-image'
            ),
            'nonAvailableOptionsTextPlacement'           => Advanced_Product_Selector::$options->get( 'aps-setting-non-available-options-text-placement' ),
        ];

        $page_embed_data['txt'] = [
            'select'                   => __( 'Select {}', 'aps' ),
            'paginationPrevious'       => __( 'Previous', 'aps' ),
            'paginationNext'           => __( 'Next', 'aps' ),
            'yes'                      => __( 'Yes', 'aps' ),
            'no'                       => __( 'No', 'aps' ),
            'next'                     => __( 'Next', 'aps' ),
            'finish'                   => __( 'Finish', 'aps' ),
            'confirmSelectionQuestion' => __( 'Select {$1} "{$2}"?', 'aps' ),
            'selectAttributes'         => __( 'Select attributes', 'aps' ),
            'firstChooseAttr'          => __( 'First choose {}', 'aps' ),
        ];

        $page_embed_data['pricesTemplate'] = Attributes_Prices::get_prices_strings();

        // Adds the data for attributes for the current variable product.

        wp_localize_script(
            'aps-public-script',
            'advancedProductSelector',
            $page_embed_data
        );
    }
}
