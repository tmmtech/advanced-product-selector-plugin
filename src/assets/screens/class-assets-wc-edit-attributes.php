<?php
/**
 * Assets WC Edit Attributes screen (/wp-admin/edit.php?post_type=product&page=product_attributes) customization.
 *
 * Loads assets (JS, CSS), adds data for them.
 *
 * @package Advanced-Product-Selector
 * @since   0.0.1
 */

namespace Advanced_Product_Selector\Assets\Menu\Screens;

use Advanced_Product_Selector\Admin_Menu;

/**
 * Assets class.
 */
final class WC_Edit_Attributes {

    /**
     * Constructor.
     */
    public function __construct() {
        $this->scripts();
    }

    /**
     * Loads scripts.
     */
    private function scripts() {
        wp_enqueue_script(
            'aps-admin-wc-edit-attributes-screen-script',
            ADVANCED_PRODUCT_SELECTOR_URL . 'assets-build/admin/screens/wc-edit-attributes.js',
            [],
            ADVANCED_PRODUCT_SELECTOR_VERSION,
            true
        );

        wp_localize_script(
            'aps-admin-wc-edit-attributes-screen-script',
            'advancedProductSelector',
            [
                'txt' => Admin_Menu::get_texts(),
            ]
        );
    }
}
