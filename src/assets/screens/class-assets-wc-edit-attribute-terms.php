<?php
/**
 * Assets WC Edit Attribute Terms screen (/wp-admin/edit-tags.php?taxonomy=pa_brand&post_type=product) customization.
 *
 * Loads assets (JS, CSS), adds data for them.
 *
 * @package Advanced-Product-Selector
 * @since   0.0.1
 */

namespace Advanced_Product_Selector\Assets\Menu\Screens;

use Advanced_Product_Selector\Admin_Menu;

/**
 * Assets class.
 */
final class WC_Edit_Attribute_Terms {

    /**
     * Constructor.
     */
    public function __construct() {
        $this->scripts();
    }

    /**
     * Loads scripts.
     */
    private function scripts() {
        wp_enqueue_script(
            'aps-admin-wc-edit-attributes-screen-script',
            ADVANCED_PRODUCT_SELECTOR_URL . 'assets-build/admin/screens/wc-edit-attribute-terms.js',
            [],
            ADVANCED_PRODUCT_SELECTOR_VERSION,
            true
        );

        $taxonomy = preg_replace( '@^edit-@', '', get_current_screen()->id );
        $terms    = get_terms( $taxonomy, [ 'hide_empty' => false ] );
        $term_ids = array_column( $terms, 'slug', 'term_id' );

        wp_localize_script(
            'aps-admin-wc-edit-attributes-screen-script',
            'advancedProductSelector',
            [
                'txt'      => Admin_Menu::get_texts(),
                'termsIds' => $term_ids,
            ]
        );
    }
}
