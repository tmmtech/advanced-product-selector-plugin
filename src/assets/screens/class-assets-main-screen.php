<?php
/**
 * Assets for main screen
 *
 * Loads assets (JS, CSS), adds data for them.
 *
 * @package Advanced-Product-Selector
 * @since   0.0.1
 */

namespace Advanced_Product_Selector\Assets\Menu\Screens;

use Advanced_Product_Selector\Admin_Menu;

/**
 * Assets class.
 */
final class Main {

    /**
     * Inits.
     */
    public static function init() {
        $class = __CLASS__;
        add_action(
            'admin_enqueue_scripts',
            function () use ( $class ) {
                new $class();
            }
        );
    }

    /**
     * Constructor.
     */
    public function __construct() {
        $this->styles();
        $this->scripts();
    }

    /**
     * Loads styles.
     */
    private function styles() {
        wp_register_style(
            'aps-admin-style',
            ADVANCED_PRODUCT_SELECTOR_URL . 'assets-build/admin/index.css',
            [],
            ADVANCED_PRODUCT_SELECTOR_VERSION
        );

        wp_enqueue_style(
            'aps-admin-main-screen-style',
            ADVANCED_PRODUCT_SELECTOR_URL . 'assets-build/admin/screens/main.css',
            [ 'aps-admin-style', 'tmm-wp-plugins-core-admin-style' ],
            ADVANCED_PRODUCT_SELECTOR_VERSION
        );
    }

    /**
     * Loads scripts.
     */
    private function scripts() {
        // Core media assets (for image upload).

        wp_enqueue_media();

        // Scripts.

        wp_register_script(
            'aps-admin-script',
            ADVANCED_PRODUCT_SELECTOR_URL . 'assets-build/admin/index.js',
            [],
            ADVANCED_PRODUCT_SELECTOR_VERSION,
            true
        );

        // SortableJS.

        wp_register_script(
            'aps-lib-sortablejs',
            ADVANCED_PRODUCT_SELECTOR_URL . 'libs/sortablejs/Sortable.min.js',
            [],
            '1.14.0',
            true
        );

        // Main script.

        wp_enqueue_script(
            'aps-admin-main-screen-script',
            ADVANCED_PRODUCT_SELECTOR_URL . 'assets-build/admin/screens/main.js',
            [ 'aps-admin-script', 'aps-lib-sortablejs', 'tmm-wp-plugins-core-admin-script' ],
            ADVANCED_PRODUCT_SELECTOR_VERSION,
            true
        );

        wp_localize_script(
            'aps-admin-main-screen-script',
            'advancedProductSelector',
            [
                'nonceToken' => wp_create_nonce( 'aps-menu' ),
                'txt'        => Admin_Menu::get_texts(),
            ]
        );
    }
}
