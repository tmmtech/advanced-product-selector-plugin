<?php
/**
 * Assets for attr screen
 *
 * Loads assets (JS, CSS), adds data for them.
 *
 * @package Advanced-Product-Selector
 * @since   0.0.1
 */

namespace Advanced_Product_Selector\Assets\Menu\Screens;

use Advanced_Product_Selector\Admin_Menu;

/**
 * Assets class.
 */
final class Term {

    /**
     * Inits.
     */
    public static function init() {
        $class = __CLASS__;
        add_action(
            'admin_enqueue_scripts',
            function () use ( $class ) {
                new $class();
            }
        );
    }

    /**
     * Constructor.
     */
    public function __construct() {
        $this->styles();
        $this->scripts();
    }

    /**
     * Loads styles.
     */
    private function styles() {

        // Pickr.

        wp_register_style(
            'aps-lib-pickr',
            ADVANCED_PRODUCT_SELECTOR_URL . 'libs/pickr/themes/classic.min.css',
            [],
            '1.8.2'
        );

        wp_register_style(
            'aps-admin-style',
            ADVANCED_PRODUCT_SELECTOR_URL . 'assets-build/admin/index.css',
            [],
            ADVANCED_PRODUCT_SELECTOR_VERSION
        );

        wp_enqueue_style(
            'aps-admin-main-screen-style',
            ADVANCED_PRODUCT_SELECTOR_URL . 'assets-build/admin/screens/term.css',
            [ 'aps-admin-style', 'aps-lib-pickr', 'tmm-wp-plugins-core-admin-style' ],
            ADVANCED_PRODUCT_SELECTOR_VERSION
        );
    }

    /**
     * Loads scripts.
     */
    private function scripts() {
        // Core media assets (for image upload).

        wp_enqueue_media();

        // Pickr.

        wp_register_script(
            'aps-lib-pickr',
            ADVANCED_PRODUCT_SELECTOR_URL . 'libs/pickr/pickr.min.js',
            [],
            '1.8.2',
            true
        );

        // Scripts.

        wp_register_script(
            'aps-admin-script',
            ADVANCED_PRODUCT_SELECTOR_URL . 'assets-build/admin/index.js',
            [],
            ADVANCED_PRODUCT_SELECTOR_VERSION,
            true
        );

        // Main script.

        wp_enqueue_script(
            'aps-admin-main-screen-script',
            ADVANCED_PRODUCT_SELECTOR_URL . 'assets-build/admin/screens/term.js',
            [ 'aps-admin-script', 'aps-lib-pickr', 'tmm-wp-plugins-core-admin-script' ],
            ADVANCED_PRODUCT_SELECTOR_VERSION,
            true
        );

        wp_localize_script(
            'aps-admin-main-screen-script',
            'advancedProductSelector',
            [
                'nonceToken' => wp_create_nonce( 'aps-menu' ),
                'txt'        => Admin_Menu::get_texts(),
            ]
        );
    }
}
