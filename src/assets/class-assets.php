<?php
/**
 * Assets
 *
 * Main assets' handler.
 *
 * @package Advanced-Product-Selector
 * @since   0.0.1
 */

namespace Advanced_Product_Selector\Assets;

/**
 * Assets class.
 */
final class Assets {


    /**
     * Main init.
     */
    public static function init() {
        // Public assets.

        add_action(
            'wp_enqueue_scripts',
            [ 'Advanced_Product_Selector\Assets\Publ', 'init' ]
        );

        // Edit WC attributes.

        add_action(
            'current_screen',
            function () {
                $screen = get_current_screen();

                if (
                    'product_page_product_attributes' === $screen->id &&
                    ! isset( $_GET['edit'] ) // @codingStandardsIgnoreLine
                ) {
                    new Menu\Screens\WC_Edit_Attributes;
                }
            }
        );

        // Edit WC attribute terms.

        add_action(
            'current_screen',
            function () {
                $screen = get_current_screen();

                if ( str_starts_with( $screen->id, 'edit-pa_' ) &&
                'edit-tags' === $screen->base &&
                'product' === $screen->post_type
                ) {
                    new Menu\Screens\WC_Edit_Attribute_Terms;
                }
            }
        );
    }
}
