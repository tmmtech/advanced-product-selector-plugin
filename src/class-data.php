<?php
/**
 * A class to handle data.
 *
 * @package Advanced-Product-Selector
 * @since   0.0.1
 */

namespace Advanced_Product_Selector;

/**
 * Data class.
 */
final class Data {

    /**
     * Returns attribute meta-data.
     *
     * Also compensates the difference (if any new fields were added,
     * but the saved meta lacks them, then returns saved meta with them).
     *
     * @param int $attribute_id Attribute ID.
     *
     * @return array Attribute data.
     */
    public static function get_attribute_data( $attribute_id ) {
        $template = [
            'description'                           => '',
            'terms_order'                           => [],
            'number_of_columns_desktop'             => 4,
            'number_of_columns_tablet'              => 2,
            'number_of_columns_mobile'              => 1,
            'num_per_page'                          => 8,
            'skip_select_confirmation'              => true,
            'show_price_per_attribute'              => true,
            'recommended_terms'                     => [],
            'text_for_non_available_options'        => __( 'This option is not available with the prior selected options', 'aps' ),
            'auto_select_the_only_available_option' => true,
        ];

        $attribute_meta = get_term_meta( $attribute_id, 'advanced-product-selector', true );

        if ( is_array( $attribute_meta ) ) {
            $missed = array_diff_key( $template, $attribute_meta );
            $out    = array_merge( $attribute_meta, $missed );
        } else {
            $out = $template;
        }

        return $out;
    }

    /**
     * Returns attribute term meta-data.
     *
     * Also compensates the difference (if any new fields were added,
     * but the saved meta lacks them, then returns saved meta with them).
     *
     * @param int $term_id Term ID.
     *
     * @return array Term data.
     */
    public static function get_term_meta_data( $term_id ) {
        $template = [
            'tooltip'                  => '',
            'image'                    => '', // Attachment ID.
            'image-hover'              => '', // Attachment ID.
            'recommended_text'         => __( 'Recommended', 'aps' ),
            'recommended_border_color' => 'rgba(125, 189, 255, 1)',
        ];

        $term_meta = get_term_meta( $term_id, 'advanced-product-selector', true );

        if ( is_array( $term_meta ) ) {
            $missed = array_diff_key( $template, $term_meta );
            $out    = array_merge( $term_meta, $missed );
        } else {
            $out = $template;
        }

        return $out;
    }

    /**
     * Returns attribute term data.
     *
     * Also compensates the difference (if any new fields were added,
     * but the saved meta lacks them, then returns saved meta with them).
     *
     * @param int $term_id Term ID.
     *
     * @return array Term data.
     */
    public static function get_term_data( $term_id ) {
        return array_merge(
            self::get_term_meta_data( $term_id ),
            [ 'description' => term_description( $term_id ) ]
        );
    }

    /**
     * Returns image data.
     *
     * @param int    $attachment_id Attachment ID.
     * @param string $size          Size.
     *
     * @return array|false
     */
    public static function get_image_data( $attachment_id, $size = 'large' ) {
        if ( ! $attachment_id ) {
            return false;
        }

        $image = wp_get_attachment_image_src( $attachment_id, $size );

        if ( false === $image ) {
            return false;
        }

        $image_alt = get_post_meta( $attachment_id, '_wp_attachment_image_alt', true );

        return [
            'src' => $image[0],
            'alt' => $image_alt,
        ];
    }

    /**
     * Returns the total count of terms the attribute has.
     *
     * @param string $attribute_slug Attribute slug.
     *
     * @return int
     */
    public static function get_terms_count( $attribute_slug ) {
        $result = wp_count_terms( [ 'taxonomy' => "pa_$attribute_slug" ] );
        if ( is_wp_error( $result ) ) {
            return 0;
        } else {
            return (int) $result;
        }
    }

    /**
     * Returns the count of configured terms for the attribute.
     *
     * Configured terms are the terms that have an image.
     *
     * @param string $attribute_slug Attribute slug.
     *
     * @return int
     */
    public static function get_configured_terms_count( $attribute_slug ) {
        $terms    = get_terms( "pa_$attribute_slug", [ 'hide_empty' => false ] );
        $term_ids = array_column( $terms, 'term_id', 'slug' );

        $configured_terms_count = 0;
        foreach ( $term_ids as $term_id ) {
            $term_data = self::get_term_meta_data( $term_id );
            if ( self::get_image_data( $term_data['image'], 'thumbnail' ) ) {
                $configured_terms_count++;
            }
        }

        return $configured_terms_count;
    }

    /**
     * Returns attribute terms meta values.
     *
     * @param string $attribute Attribute.
     *
     * @return array Attribute terms advanced-product-selector meta values.
     */
    public static function get_attribute_terms_meta_values( $attribute ) {
        global $wpdb;

        $results = $wpdb->get_results(
            "                            
                SELECT t.slug, tm.meta_value
                FROM {$wpdb->prefix}term_taxonomy as tt
                INNER JOIN {$wpdb->prefix}terms as t ON tt.term_id = t.term_id
                INNER JOIN {$wpdb->prefix}termmeta as tm ON tm.term_id = t.term_id
                WHERE tt.taxonomy = 'pa_$attribute'
                AND tm.meta_key = 'advanced-product-selector';
                ",
            ARRAY_A
        );

        foreach ( $results as &$result ) { // Unserialize.
            $result['meta_value'] = unserialize( $result['meta_value'] ); // @codingStandardsIgnoreLine
        }

        // Format the data for merge.
        foreach ( $results as &$result ) {
            $result['data']               = [];
            $result['data']['meta_value'] = $result['meta_value']; // @codingStandardsIgnoreLine
            unset( $result['meta_value'] );
        }

        return $results;
    }

    /**
     * Returns attribute terms description.
     *
     * @param string $attribute Attribute.
     *
     * @return array Attribute terms description.
     */
    public static function get_attribute_terms_description( $attribute ) {
        global $wpdb;

        $results = $wpdb->get_results(
            "
                SELECT t.slug, tt.description
                FROM {$wpdb->prefix}term_taxonomy as tt
                INNER JOIN {$wpdb->prefix}terms as t ON tt.term_id = t.term_id
                WHERE tt.taxonomy = 'pa_$attribute'
				AND TRIM(tt.description) != '';
                ",
            ARRAY_A
        );

        // Format the data for merge.
        foreach ( $results as &$result ) {
            $result['data']                = [];
            $result['data']['description'] = $result['description']; // @codingStandardsIgnoreLine
            unset( $result['description'] );
        }

        return $results;
    }
}
