<?php
/**
 * A class to get the variation attributes prices.
 *
 * @package Advanced-Product-Selector
 * @since   0.0.1
 */

namespace Advanced_Product_Selector;

/**
 * Variation Prices class.
 */
final class Attributes_Prices {

    /**
     * Returns attributes prices string. For one price, and for two (range).
     *
     * Example:
     *
     * [
     *     "one" => <span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">$</span>12.34</bdi></span>,
     *     "two" => <span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">$</span>12.34</bdi></span> –
     *              <span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">$</span>98.76</bdi></span>
     * ]
     *
     * @return array Attributes prices string.
     */
    public static function get_prices_strings() {
        return [
            'one'          => wc_price( 12.34 ),
            'two'          => wc_format_price_range( wc_price( 12.34 ), wc_price( 98.76 ) ),
            'decimal_sep'  => get_option( 'woocommerce_price_decimal_sep' ),
            'thousand_sep' => get_option( 'woocommerce_price_thousand_sep' ),
            'num_decimals' => get_option( 'woocommerce_price_num_decimals' ),
        ];
    }
}
