<?php
/**
 * AJAX handlers.
 *
 * @package Advanced-Product-Selector
 * @since   0.0.1
 */

namespace Advanced_Product_Selector;

use Exception;
use WP_Plugins_Core\Sanitize;

/**
 * Manages AJAX.
 */
final class AJAX {

    /**
     * Adds the menu and inits assets loading for it.
     */
    public function __construct() {
        $this->add_ajax_events();
    }

    /**
     * Loads AJAX handlers.
     */
    private function add_ajax_events() {
        $admin_ajax_events = [
            'save',
            'save_attr',
            'save_term',
            'import_settings',
            'reset_settings',
        ];

        foreach ( $admin_ajax_events as $ajax_event ) {
            add_action( 'wp_ajax_advanced_product_selector_' . $ajax_event, [ $this, $ajax_event ] );
        }
    }

    /**
     * Save plugin settings.
     *
     * @throws Exception Exception.
     */
    public function save() {
        /*
         * Nonce check.
         */
        check_ajax_referer( 'aps-menu', 'nonceToken' );

        if ( array_key_exists( 'useSpecifiedOrder', $_REQUEST ) ) {
            $use_specified_order = sanitize_text_field( wp_unslash( $_REQUEST['useSpecifiedOrder'] ) ) === 'on' ? '1' : '';
            Advanced_Product_Selector::$options->set( 'aps-setting-use-specified-order', $use_specified_order );
        }

        if ( array_key_exists( 'attributesOrder', $_REQUEST ) ) {
            $attributes_order = Sanitize::sanitize_array( (array) json_decode( wp_unslash( $_REQUEST['attributesOrder'] ), true ) ); // @codingStandardsIgnoreLine
            Advanced_Product_Selector::$options->set( 'aps-setting-attributes-order', $attributes_order );
        }

        if ( array_key_exists( 'advancedSelectorAttributes', $_REQUEST ) ) {
            $advanced_selector_attributes =
                Sanitize::sanitize_array( (array) json_decode( wp_unslash( $_REQUEST['advancedSelectorAttributes'] ), true ) ); // @codingStandardsIgnoreLine
            Advanced_Product_Selector::$options->set( 'aps-setting-advanced-selector-attributes', $advanced_selector_attributes );
        }

        if ( array_key_exists( 'showAllAttributesInOneModal', $_REQUEST ) ) {
            $show_all_attributes_in_one_modal = sanitize_text_field( wp_unslash( $_REQUEST['showAllAttributesInOneModal'] ) ) === 'on' ? '1' : '';
            Advanced_Product_Selector::$options->set( 'aps-setting-show-all-attributes-in-one-modal', $show_all_attributes_in_one_modal );
        }

        if ( array_key_exists( 'selectAttributesStepByStep', $_REQUEST ) ) {
            $select_attributes_step_by_step = sanitize_text_field( wp_unslash( $_REQUEST['selectAttributesStepByStep'] ) ) === 'on' ? '1' : '';
            Advanced_Product_Selector::$options->set( 'aps-setting-select-attributes-step-by-step', $select_attributes_step_by_step );
        }

        if ( array_key_exists( 'attributesStepByStepSelectionText', $_REQUEST ) ) {
            $step_by_step_selection_text = sanitize_text_field( wp_unslash( $_REQUEST['attributesStepByStepSelectionText'] ) );
            Advanced_Product_Selector::$options->set( 'aps-setting-attributes-step-by-step-selection-text', $step_by_step_selection_text );
        }

        if ( array_key_exists( 'hideNotConfiguredTerms', $_REQUEST ) ) {
            $hide_not_configured_terms = sanitize_text_field( wp_unslash( $_REQUEST['hideNotConfiguredTerms'] ) ) === 'on' ? '1' : '';
            Advanced_Product_Selector::$options->set( 'aps-setting-hide-not-configured-terms', $hide_not_configured_terms );
        }

        if ( array_key_exists( 'deselectOptionOnTheClick', $_REQUEST ) ) {
            $deselect_option_on_the_click = sanitize_text_field( wp_unslash( $_REQUEST['deselectOptionOnTheClick'] ) ) === 'on' ? '1' : '';
            Advanced_Product_Selector::$options->set( 'aps-setting-deselect-option-on-the-click', $deselect_option_on_the_click );
        }

        if ( array_key_exists( 'automaticallyShowTheNextAttributeModal', $_REQUEST ) ) {
            $automatically_show_the_next_attribute_modal = sanitize_text_field( wp_unslash( $_REQUEST['automaticallyShowTheNextAttributeModal'] ) ) === 'on' ? '1' : '';
            Advanced_Product_Selector::$options->set( 'aps-setting-automatically-show-the-next-attribute-modal', $automatically_show_the_next_attribute_modal );
        }

        if ( array_key_exists( 'howToTreatUnavailableAttributeCombinations', $_REQUEST ) ) {
            $how_to_treat_unavailable_attribute_combinations = sanitize_text_field( wp_unslash( $_REQUEST['howToTreatUnavailableAttributeCombinations'] ) );
            Advanced_Product_Selector::$options->set( 'aps-setting-how-to-treat-unavailable-attribute-combinations', $how_to_treat_unavailable_attribute_combinations );
        }

        if ( array_key_exists( 'nonAvailableOptionsTextPlacement', $_REQUEST ) ) {
            $non_available_options_text_placement = sanitize_text_field( wp_unslash( $_REQUEST['nonAvailableOptionsTextPlacement'] ) );
            Advanced_Product_Selector::$options->set( 'aps-setting-non-available-options-text-placement', $non_available_options_text_placement );
        }

        if ( array_key_exists( 'arrowImage', $_REQUEST ) ) {
            $arrow_image = sanitize_text_field( wp_unslash( $_REQUEST['arrowImage'] ) );
            Advanced_Product_Selector::$options->set( 'aps-setting-custom-progress-bar-arrow', $arrow_image );
        } else {
            Advanced_Product_Selector::$options->delete( 'aps-setting-custom-progress-bar-arrow' );
        }

        wp_send_json_success();
    }

    /**
     * Save attribute settings.
     *
     * @throws Exception Exception.
     */
    public function save_attr() {
        /*
         * Nonce check.
         */
        check_ajax_referer( 'aps-menu', 'nonceToken' );

        if ( ! array_key_exists( 'attrID', $_REQUEST ) || ! array_key_exists( 'attrSlug', $_REQUEST ) ) {
            wp_send_json_success( [ 'data' => 'No attrID passed!' ] );
        }

        $attr_id   = (int) sanitize_text_field( wp_unslash( $_REQUEST['attrID'] ) );
        $attr_slug = sanitize_text_field( wp_unslash( $_REQUEST['attrSlug'] ) );

        $data = [];

        if ( array_key_exists( 'description', $_REQUEST ) ) {
            $data['description'] = sanitize_text_field( wp_unslash( $_REQUEST['description'] ) );
        }

        if ( array_key_exists( 'termsOrder', $_REQUEST ) ) {
            $data['terms_order'] =
                Sanitize::sanitize_array( (array) json_decode( wp_unslash( $_REQUEST['termsOrder'] ), true ) ); // @codingStandardsIgnoreLine
        }

        if ( array_key_exists( 'recommendedTerms', $_REQUEST ) ) {
            $data['recommended_terms'] =
                Sanitize::sanitize_array( (array) json_decode( wp_unslash( $_REQUEST['recommendedTerms'] ), true ) ); // @codingStandardsIgnoreLine
        }

        if ( array_key_exists( 'numberOfColumnsDesktop', $_REQUEST ) ) {
            $data['number_of_columns_desktop'] = (int) sanitize_text_field( wp_unslash( $_REQUEST['numberOfColumnsDesktop'] ) );
        }

        if ( array_key_exists( 'numberOfColumnsTablet', $_REQUEST ) ) {
            $data['number_of_columns_tablet'] = (int) sanitize_text_field( wp_unslash( $_REQUEST['numberOfColumnsTablet'] ) );
        }

        if ( array_key_exists( 'numberOfColumnsMobile', $_REQUEST ) ) {
            $data['number_of_columns_mobile'] = (int) sanitize_text_field( wp_unslash( $_REQUEST['numberOfColumnsMobile'] ) );
        }

        if ( array_key_exists( 'numPerPage', $_REQUEST ) ) {
            $data['num_per_page'] = (int) sanitize_text_field( wp_unslash( $_REQUEST['numPerPage'] ) );
        }

        if ( array_key_exists( 'skipSelectConfirmation', $_REQUEST ) ) {
            $data['skip_select_confirmation'] = sanitize_text_field( wp_unslash( $_REQUEST['skipSelectConfirmation'] ) ) === 'on';
        }

        if ( array_key_exists( 'showPricePerAttribute', $_REQUEST ) ) {
            $data['show_price_per_attribute'] = sanitize_text_field( wp_unslash( $_REQUEST['showPricePerAttribute'] ) ) === 'on';
        }

        if ( array_key_exists( 'textForNonAvailableOptions', $_REQUEST ) ) {
            $data['text_for_non_available_options'] = sanitize_text_field( wp_unslash( $_REQUEST['textForNonAvailableOptions'] ) );
        }

        if ( array_key_exists( 'autoSelectTheOnlyAvailableOption', $_REQUEST ) ) {
            $data['auto_select_the_only_available_option'] = sanitize_text_field( wp_unslash( $_REQUEST['autoSelectTheOnlyAvailableOption'] ) ) === 'on';
        }

        if ( array_key_exists( 'attributeAdvancedSelector', $_REQUEST ) ) {
            $advanced_selector_attributes = Advanced_Product_Selector::$options->get( 'aps-setting-advanced-selector-attributes' );

            $is_advanced_selector_attribute = sanitize_text_field( wp_unslash( $_REQUEST['attributeAdvancedSelector'] ) ) === 'on';

            if ( ! $is_advanced_selector_attribute && in_array( $attr_slug, $advanced_selector_attributes, true ) ) {
                unset( $advanced_selector_attributes[ array_search( $attr_slug, $advanced_selector_attributes, true ) ] );
            } elseif ( $is_advanced_selector_attribute &&
                ! in_array( $attr_slug, $advanced_selector_attributes, true )
            ) {
                $advanced_selector_attributes[] = $attr_slug;
            }
            Advanced_Product_Selector::$options->set( 'aps-setting-advanced-selector-attributes', $advanced_selector_attributes );
        }

        update_term_meta( $attr_id, 'advanced-product-selector', $data );

        wp_send_json_success();
    }

    /**
     * Save attribute settings.
     */
    public function save_term() {
        /*
         * Nonce check.
         */
        check_ajax_referer( 'aps-menu', 'nonceToken' );

        if ( ! array_key_exists( 'termID', $_REQUEST ) ) {
            wp_send_json_success( [ 'data' => 'No termID passed!' ] );
        }

        if ( ! array_key_exists( 'taxonomy', $_REQUEST ) ) {
            wp_send_json_success( [ 'data' => 'No taxonomy passed!' ] );
        }

        $term_id  = (int) sanitize_text_field( wp_unslash( $_REQUEST['termID'] ) );
        $taxonomy = sanitize_text_field( wp_unslash( $_REQUEST['taxonomy'] ) );

        $data = [];

        if ( array_key_exists( 'description', $_REQUEST ) ) {
            $description = sanitize_text_field( wp_unslash( $_REQUEST['description'] ) );
            wp_update_term( $term_id, $taxonomy, [ 'description' => $description ] );
        }

        if ( array_key_exists( 'tooltip', $_REQUEST ) ) {
            $data['tooltip'] = sanitize_text_field( wp_unslash( $_REQUEST['tooltip'] ) );
        }

        if ( array_key_exists( 'image', $_REQUEST ) ) {
            $data['image'] = sanitize_text_field( wp_unslash( $_REQUEST['image'] ) );
        }

        if ( array_key_exists( 'imageHover', $_REQUEST ) ) {
            $data['image-hover'] = sanitize_text_field( wp_unslash( $_REQUEST['imageHover'] ) );
        }

        if ( array_key_exists( 'recommendedText', $_REQUEST ) ) {
            $data['recommended_text'] = sanitize_text_field( wp_unslash( $_REQUEST['recommendedText'] ) );
        }

        if ( array_key_exists( 'recommendedBorderColor', $_REQUEST ) ) {
            $data['recommended_border_color'] = sanitize_text_field( wp_unslash( $_REQUEST['recommendedBorderColor'] ) );
        }

        update_term_meta( $term_id, 'advanced-product-selector', $data );

        wp_send_json_success();
    }

    /**
     * Imports settings from the file.
     *
     * Accepts: $_REQUEST['file']
     *          $_REQUEST['type']
     *
     * @throws Exception Exception.
     */
    public function import_settings() {
        /*
         * Nonce check.
         */
        check_ajax_referer( 'aps-menu', 'nonceToken' );

        if (
            array_key_exists( 'file', $_REQUEST ) &&
            array_key_exists( 'type', $_REQUEST )
        ) {
            Settings_Import::import( sanitize_text_field( wp_unslash( $_REQUEST['file'] ) ), sanitize_text_field( wp_unslash( $_REQUEST['type'] ) ) );
            wp_send_json_success();
        } else {
            wp_send_json_error();
        }
    }

    /**
     * Resets all plugin settings.
     *
     * @throws Exception Exception.
     */
    public function reset_settings() {
        /*
         * Nonce check.
         */
        check_ajax_referer( 'aps-menu', 'nonceToken' );

        // Delete all settings options.

        foreach ( Advanced_Product_Selector::$options->settings_options as $option ) {
            Advanced_Product_Selector::$options->delete( $option );

            // Set them to defaults to trigger all event listeners.
            $option_data = Advanced_Product_Selector::$options->all_options[ $option ];
            if ( array_key_exists( 'default', $option_data ) ) {
                Advanced_Product_Selector::$options->set( $option, $option_data['default'] );
            }
        }

        // Delete attributes and attribute terms meta.

        $attributes = wc_get_attribute_taxonomies();
        foreach ( $attributes as $attribute ) {
            $attribute_name = $attribute->attribute_name;

            delete_term_meta( $attribute->attribute_id, 'advanced-product-selector' );

            foreach ( array_column( Data::get_attribute_terms_meta_values( $attribute_name ), 'slug' ) as $term_slug ) {
                $term = get_term_by( 'slug', $term_slug, "pa_$attribute_name" ); // Search for the term by slug.

                if ( ! $term || is_wp_error( $term ) ) {
                    continue;
                }

                delete_term_meta( $term->term_id, 'advanced-product-selector' );
            }
        }

        wp_send_json_success();
    }
}
