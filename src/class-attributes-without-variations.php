<?php
/**
 * A class to get the list of attributes without variations.
 *
 * Because there is no sense in showing configuration for such attributes.
 *
 * @package Advanced-Product-Selector
 * @since   0.0.1
 */

namespace Advanced_Product_Selector;

/**
 * Attributes_Without_Variations class.
 */
final class Attributes_Without_Variations {

    /**
     * Finds the list of attributes without variations and saves them to the plugin option.
     */
    public static function get_attributes_without_variations() {
        $attributes_without_variations = [];

        $attributes = wc_get_attribute_taxonomies();

        foreach ( $attributes as $attribute ) {
            $attribute_name = $attribute->attribute_name;

            global $wpdb;

            $attribute_variations_count = (int) $wpdb->get_var( "
                SELECT 1
                    FROM {$wpdb->prefix}posts as p
                    WHERE p.post_type = 'product_variation'
                    AND p.post_status = 'publish'
                    AND p.post_parent IN (
                        SELECT DISTINCT p.ID
                            FROM {$wpdb->prefix}posts as p
                            INNER JOIN {$wpdb->prefix}posts as p2 ON p2.post_parent = p.ID
                            INNER JOIN {$wpdb->prefix}term_relationships as tr ON p.ID = tr.object_id
                            INNER JOIN {$wpdb->prefix}term_taxonomy as tt ON tr.term_taxonomy_id = tt.term_taxonomy_id
                            INNER JOIN {$wpdb->prefix}terms as t ON tt.term_id = t.term_id
                            WHERE p.post_type = 'product'
                                  AND p.post_status = 'publish'
                                      AND p2.post_status = 'publish'
                                          AND tt.taxonomy = 'pa_$attribute_name'
                    )
                    LIMIT 1;
            " );

            if ( 1 !== $attribute_variations_count ) {
                $attributes_without_variations[] = $attribute_name;
            }
        }

        return $attributes_without_variations;
    }
}
