<?php
/**
 * Makes WC a dependency (a requirement) to start the plugin.
 *
 * @package Advanced-Product-Selector
 * @since   0.0.1
 */

namespace Advanced_Product_Selector;

/**
 * Class WC Dependency.
 */
final class WC_Dependency {


    const WC_FILE = 'woocommerce/woocommerce.php';

    /**
     * Init.
     */
    public static function add() {
        add_action(
            'init',
            function () {
                // Deactivates the plugin on activation if WC is not active.
                if (
                    is_admin() &&
                    current_user_can( 'activate_plugins' ) &&
                    function_exists( 'is_plugin_active' ) &&
                    ! is_plugin_active( self::WC_FILE )
                ) {
                    self::deactivate_advanced_product_selector_plugin();
                    if ( isset( $_GET['activate'] ) ) {
                        add_action( 'admin_notices', [ 'Advanced_Product_Selector\WC_Dependency', 'wc_is_not_active_admin_notice' ] );
                        unset( $_GET['activate'] );
                    }
                }
            }
        );
    }

    /**
     * WC is not active error notice.
     */
    public static function deactivate_advanced_product_selector_plugin() {
        deactivate_plugins( ADVANCED_PRODUCT_SELECTOR_BASENAME );
    }

    /**
     * WC is not active error notice.
     */
    public static function wc_is_not_active_admin_notice() {
        echo '<div class="notice notice-error is-dismissible">
            <p>' . __( 'To activate Advanced Product Selector plugin, you must have WooCommerce plugin active.', 'aps' ) . '</p>
        </div>';
    }

    /**
     * Check if WC is active and installed.
     *
     * @return bool Whether the WC is active and installed.
     */
    public static function is_wc_installed_and_active() {
        return self::is_plugin_file_exist() && self::is_plugin_active();
    }

    /**
     * Checks whether the plugin file exists.
     *
     * @return bool
     */
    public static function is_plugin_file_exist() {
        return Advanced_Product_Selector::$fs->is_file( trailingslashit( WP_PLUGIN_DIR ) . self::WC_FILE );
    }

    /**
     * Checks whether the plugin is active.
     *
     * @return bool
     */
    public static function is_plugin_active() {
        return in_array( self::WC_FILE, apply_filters( 'active_plugins', get_option( 'active_plugins' ) ), true );
    }
}
