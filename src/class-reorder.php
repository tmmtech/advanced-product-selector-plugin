<?php
/**
 * A class to handle the logic for attributes and terms ordering.
 *
 * @package Advanced-Product-Selector
 * @since   0.0.1
 */

namespace Advanced_Product_Selector;

use Exception;

/**
 * Reorder class.
 */
final class Reorder {


    /**
     * Constructor.
     *
     * Handles main logic.
     */
    public function __construct() {
        /**
         * Reorders attributes on variable product view.
         */
        add_filter(
            'woocommerce_product_get_attributes',
            [ $this, 'reorder_hook_attributes' ]
        );
    }

    /**
     * Reorders attributes for variable product view.
     *
     * NOTE: But does not reorder attribute terms! Attribute terms order is handled for advanced-selector attributes only.
     *
     * @param array $attributes Attributes.
     *
     * @return array
     * @throws Exception Exception.
     */
    public function reorder_hook_attributes( $attributes ) {
        if ( '' === Advanced_Product_Selector::$options->get( 'aps-setting-use-specified-order' ) ) {
            return $attributes;
        }

        $attributes_order = array_map(
            function ( $x ) {
                return 'pa_' . $x;
            },
            Advanced_Product_Selector::$options->get( 'aps-setting-attributes-order' )
        );

        return $this->reorder_attributes( $attributes, $attributes_order );
    }

    /**
     * Reorders attributes.
     *
     * @param array      $attributes       Attributes.
     * @param array|bool $attributes_order Attributes order.
     *
     * @return array
     * @throws Exception Exception.
     */
    public function reorder_attributes( $attributes, $attributes_order = false ) {
        if ( false === $attributes_order ) {
            $attributes_order = Advanced_Product_Selector::$options->get( 'aps-setting-attributes-order' );
        }
        return $this->reorder_entities( $attributes, $attributes_order );
    }

    /**
     * Reorders attribute terms.
     *
     * @param array $terms       Terms.
     * @param array $terms_order Terms order.
     *
     * @return array
     */
    public function reorder_terms( $terms, $terms_order ) {
        return $this->reorder_entities( $terms, $terms_order );
    }

    /**
     * Reorders entities (attributes or terms).
     *
     * @param array $entities Entities.
     * @param array $order    Order.
     *
     * @return array
     */
    private function reorder_entities( $entities, $order ) {
        $unordered_entities = array_diff_key( $entities, array_flip( $order ) );

        foreach ( $order as $ordered_entity ) {
            if ( array_key_exists( $ordered_entity, $entities ) ) {
                $v = $entities[ $ordered_entity ];
                unset( $entities[ $ordered_entity ] );
                $entities[ $ordered_entity ] = $v;
            }
        }

        foreach ( $unordered_entities as $unordered_key => $unordered_val ) { // Adds unsorted ones to the end.
            unset( $entities[ $unordered_key ] );
            $entities[ $unordered_key ] = $unordered_val;
        }

        return $entities;
    }
}
