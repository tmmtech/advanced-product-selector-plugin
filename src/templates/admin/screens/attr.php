<?php
/**
 * Template for admin menu, for attribute.
 *
 * @package Advanced-Product-Selector
 * @since   0.0.1
 */

use Advanced_Product_Selector\Advanced_Product_Selector;
use Advanced_Product_Selector\Data;
use Advanced_Product_Selector\HTML;

$attribute_slug  = $_GET['attr'];
$attributes      = wc_get_attribute_taxonomies();
$attribute_names = array_column( $attributes, 'attribute_label', 'attribute_name' );
$attribute_ids   = array_column( $attributes, 'attribute_id', 'attribute_name' );
$attribute_id    = $attribute_ids[ $attribute_slug ];
$attribute_data  = Data::get_attribute_data( $attribute_id );

$terms      = get_terms( "pa_$attribute_slug", [ 'hide_empty' => false ] );
$term_names = Advanced_Product_Selector::$attributes_order->reorder_terms( array_column( $terms, 'name', 'slug' ), $attribute_data['terms_order'] );
$term_ids   = array_column( $terms, 'term_id', 'slug' );

$advanced_selector_attributes = Advanced_Product_Selector::$options->get( 'aps-setting-advanced-selector-attributes' );

?>

<div class="wrap">
    <h1 class="aps-header">
        <?php echo str_replace( '{}', $attribute_names[ $attribute_slug ], esc_html__( 'Configure the modal for attribute "{}"', 'aps' ) ); ?>
    </h1>

    <form data-attr-id="<?php echo $attribute_id; ?>" data-attr-slug="<?php echo $attribute_slug; ?>">
        <div class="aps-row mt-m10 mb-40">
            <label>
                <input class="aps-input" name="attribute-advanced-selector" type="checkbox"
                    <?php checked( in_array( $attribute_slug, $advanced_selector_attributes, true ) ); ?>>
                <span><?php esc_html_e( 'Advanced Product Selector', 'aps' ); ?></span>
            </label>
        </div>

        <div class="aps-attribute-container">
            <h2>
                <?php esc_html_e( 'Attribute description', 'aps' ); ?>
            </h2>
            <div class="aps-column">
                <?php
                $editor_id = 'attr-description';
                wp_editor(
                    $attribute_data['description'],
                    $editor_id,
                    [
                        'textarea_name' => $editor_id,
                        'textarea_rows' => 15,
                    ]
                );
                ?>
            </div>
        </div>

        <hr>
        <h2 class="mt-30">
            <?php esc_html_e( 'Table of terms', 'aps' ); ?>
        </h2>

        <div class="tablenav top">
            <div class="alignleft actions bulkactions">
                <label for="bulk-action-selector-top" class="screen-reader-text">
                    <?php esc_html_e( 'Select bulk action' ); ?>
                </label>
                <select name="action" id="bulk-action-selector-top">
                    <option value="-1"><?php esc_html_e( 'Bulk actions' ); ?></option>
                    <option value="reset-order"><?php esc_html_e( 'Reset order', 'aps' ); ?></option>
                </select>
                <input type="submit" id="doaction" class="button action" value="Apply">
            </div>
        </div>
        <h2 class="screen-reader-text"><?php esc_html_e( 'Attributes list', 'aps' ); ?></h2>
        <table class="wp-list-table widefat fixed striped table-view-list posts">
            <thead>
            <tr>
                <td id="cb" class="manage-column column-cb check-column">
                    <!--suppress HtmlFormInputWithoutLabel -->
                    <input id="cb-select-all-1" type="checkbox">
                </td>
                <th scope="col" class="manage-column column-title column-primary">
                    <?php esc_html_e( 'Name', 'aps' ); ?>
                </th>
                <th scope="col" class="manage-column">
                    <?php esc_html_e( 'Description', 'aps' ); ?>
                </th>
                <th scope="col" class="manage-column">
                    <?php esc_html_e( 'Tooltip', 'aps' ); ?>
                </th>
                <th scope="col" class="manage-column">
                    <?php esc_html_e( 'Image', 'aps' ); ?>
                </th>
                <th scope="col" class="manage-column">
                    <?php esc_html_e( 'Recommended', 'aps' ); ?>
                </th>
                <th scope="col" class="manage-column">
                    <?php esc_html_e( 'Reorder', 'aps' ); ?><sup>1</sup>
                </th>
            </tr>
            </thead>

            <tbody id="the-list" class="aps-settings">
            <?php foreach ( $term_names as $term_slug => $term_name ) : ?>
                <?php
                $term_id   = $term_ids[ $term_slug ];
                $term_data = Data::get_term_data( $term_id );
                ?>
                <tr class="term-setting-row" data-term-slug="<?php echo esc_attr( $term_slug ); ?>">
                    <th scope="row" class="check-column">
                        <!--suppress HtmlFormInputWithoutLabel -->
                        <input name="term-<?php echo esc_attr( $term_slug ); ?>-selected" type="checkbox">
                    </th>
                    <td>
                        <a class="row-title" href="admin.php?<?php echo http_build_query( array_merge( $_GET, [ 'term' => $term_slug ] ) ); ?>" aria-label="(Edit)">
                            <?php echo esc_html( $term_name ); ?>
                        </a>
                        <div class="row-actions">
                        <span class="edit">
                            <a href="admin.php?<?php echo http_build_query( array_merge( $_GET, [ 'term' => $term_slug ] ) ); ?>" aria-label="Edit">
                                <?php esc_html_e( 'Edit' ); ?>
                            </a>
                        </span>
                        </div>
                    </td>
                    <td data-type="term-description">
                        <?php echo $term_data['description']; ?>
                    </td>
                    <td data-type="term-tooltip">
                        <?php echo $term_data['tooltip']; ?>
                    </td>
                    <td data-type="term-image">
                        <?php echo HTML::get_img( $term_data['image'] ); ?>
                    </td>
                    <td>
                        <!--suppress HtmlFormInputWithoutLabel -->
                        <input class="aps-input fake-disabled" name="term-<?php echo esc_attr( $term_slug ); ?>-recommended" disabled type="checkbox"
                            <?php checked( in_array( $term_slug, $attribute_data['recommended_terms'], true ) ); ?>>
                    </td>
                    <td class="aps-reorder-handle"></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <div class="aps-row aps-description-row">
            <div class="aps-column mt-30">
                <span>1 - <?php esc_html_e( 'But there are two exceptions which are shown before any other options: 1. Selected options; 2. Available (non-disabled/non-grayed-out) options.', 'aps' ); ?></span>
            </div>
        </div>
        <hr>
        <div class="aps-container">
            <div class="aps-row mb-15">
                <label>
                    <span><?php esc_html_e( 'Number of columns (desktop):', 'aps' ); ?></span>
                    <input class="aps-input aps-number-input" maxlength="2" name="number_of_columns_desktop" type="text"
                           value="<?php echo esc_html( $attribute_data['number_of_columns_desktop'] ); ?>">
                </label>
            </div>
            <div class="aps-row mb-15">
                <label>
                    <span><?php esc_html_e( 'Number of columns (tablet):', 'aps' ); ?></span>
                    <input class="aps-input aps-number-input" maxlength="2" name="number_of_columns_tablet" type="text"
                           value="<?php echo esc_html( $attribute_data['number_of_columns_tablet'] ); ?>">
                </label>
            </div>
            <div class="aps-row mb-15">
                <label>
                    <span><?php esc_html_e( 'Number of columns (mobile):', 'aps' ); ?></span>
                    <input class="aps-input aps-number-input" maxlength="2" name="number_of_columns_mobile" type="text"
                           value="<?php echo esc_html( $attribute_data['number_of_columns_mobile'] ); ?>">
                </label>
            </div>
            <div class="aps-row mb-15">
                <label>
                    <span><?php esc_html_e( 'Maximum number of terms per page:', 'aps' ); ?></span>
                    <input class="aps-input aps-number-input" maxlength="4" name="num_per_page" type="text"
                           value="<?php echo esc_html( $attribute_data['num_per_page'] ); ?>">
                </label>
                <p class="ml-10"><?php esc_html_e( 'Enter -1 to disable paging', 'aps' ); ?></p>
            </div>
            <div class="aps-row mb-15">
                <label>
                    <input class="aps-input" name="skip_select_confirmation" type="checkbox"
                        <?php checked( $attribute_data['skip_select_confirmation'] ); ?>>
                    <span class="p-like"><?php esc_html_e( 'Clicking the term image will select the attribute (when unchecked, the visitor needs to confirm the selection)', 'aps' ); ?></span>
                </label>
            </div>
            <div class="aps-row mb-15">
                <label>
                    <input class="aps-input" name="show_price_per_attribute" type="checkbox"
                        <?php checked( $attribute_data['show_price_per_attribute'] ); ?>>
                    <span class="p-like"><?php esc_html_e( 'Show the price per attribute (if you want to show the term differential, better use term description)', 'aps' ); ?></span>
                </label>
            </div>
            <div class="aps-row mb-15">
                <label>
                    <input class="aps-input" name="auto_select_the_only_available_option" type="checkbox"
                        <?php checked( $attribute_data['auto_select_the_only_available_option'] ); ?>>
                    <span class="p-like"><?php esc_html_e( 'Auto-select the only available option', 'aps' ); ?></span>
                </label>
            </div>
            <div class="aps-row">
                <label>
                    <span><?php esc_html_e( 'Text for non available options:', 'aps' ); ?></span>
                    <input class="aps-input aps-longtext-input" name="text_for_non_available_options" type="text"
                           value="<?php echo esc_html( $attribute_data['text_for_non_available_options'] ); ?>">
                </label>
            </div>
        </div>

        <hr>
        <div class="aps-container">
            <div class="aps-column mt-m20 mb-20">
                <?php submit_button( __( 'Save changes', 'aps' ), [ 'primary', 'aps-submit' ] ); ?>
            </div>
        </div>
    </form>
</div>
