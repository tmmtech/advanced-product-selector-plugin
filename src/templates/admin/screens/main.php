<?php
/**
 * Template for admin menu
 *
 * @package Advanced-Product-Selector
 * @since   0.0.1
 */

use Advanced_Product_Selector\Advanced_Product_Selector;
use Advanced_Product_Selector\Data;
use Advanced_Product_Selector\HTML;
use Advanced_Product_Selector\Attributes_Without_Variations;

$attributes                    = wc_get_attribute_taxonomies();
$attributes_without_variations = Attributes_Without_Variations::get_attributes_without_variations();
$attribute_names               = Advanced_Product_Selector::$attributes_order->reorder_attributes( array_column( $attributes, 'attribute_label', 'attribute_name' ) );
$advanced_selector_attributes  = Advanced_Product_Selector::$options->get( 'aps-setting-advanced-selector-attributes' );

?>

<div class="wrap">
    <h1 class="aps-header">
        <?php esc_html_e( 'Advanced Product Selector', 'aps' ); ?>
    </h1>

    <h2>
        <?php esc_html_e( 'The following product attributes are used for variable products, drag to change the order', 'aps' ); ?>
    </h2>

    <form id="posts-filter">
        <div class="tablenav top">
            <div class="alignleft actions bulkactions">
                <label for="bulk-action-selector-top" class="screen-reader-text">
                    <?php esc_html_e( 'Select bulk action' ); ?>
                </label>
                <select name="action" id="bulk-action-selector-top">
                    <option value="-1"><?php esc_html_e( 'Bulk actions' ); ?></option>
                    <option value="enable-advanced-select"><?php esc_html_e( 'Enable advanced selector', 'aps' ); ?></option>
                    <option value="disable-advanced=select"><?php esc_html_e( 'Disable advanced selector', 'aps' ); ?></option>
                    <option value="reset-order"><?php esc_html_e( 'Reset order', 'aps' ); ?></option>
                </select>
                <input type="submit" id="doaction" class="button action" value="Apply">
            </div>
        </div>
        <h2 class="screen-reader-text"><?php esc_html_e( 'Attributes list', 'aps' ); ?></h2>
        <table class="wp-list-table widefat fixed striped table-view-list posts">
            <thead>
            <tr>
                <td id="cb" class="manage-column column-cb check-column">
                    <!--suppress HtmlFormInputWithoutLabel -->
                    <input id="cb-select-all-1" type="checkbox">
                </td>
                <th scope="col" class="manage-column column-title column-primary">
                    <?php esc_html_e( 'Name', 'aps' ); ?>
                </th>
                <th scope="col" class="manage-column">
                    <?php esc_html_e( 'Slug', 'aps' ); ?>
                </th>
                <th scope="col" class="manage-column">
                    <?php esc_html_e( 'Advanced selector', 'aps' ); ?>
                </th>
                <th scope="col" class="manage-column">
                    <?php esc_html_e( 'Reorder', 'aps' ); ?><sup>1</sup>
                </th>
                <th scope="col" class="manage-column">
                    <?php esc_html_e( 'Terms configured (image set)', 'aps' ); ?>
                </th>
            </tr>
            </thead>

            <tbody id="the-list" class="aps-settings">
            <?php foreach ( $attribute_names as $attribute_slug => $attribute_name ) : ?>
                <?php
                if ( in_array( $attribute_slug, $attributes_without_variations, true ) ) {
                    continue;
                }
                ?>

                <tr class="attribute-setting-row" data-attribute-slug="<?php echo esc_attr( $attribute_slug ); ?>">
                    <th scope="row" class="check-column">
                        <!--suppress HtmlFormInputWithoutLabel -->
                        <input name="attribute-<?php echo esc_attr( $attribute_slug ); ?>-selected" type="checkbox">
                    </th>
                    <td>
                        <a class="row-title" href="admin.php?<?php echo http_build_query( array_merge( $_GET, [ 'attr' => $attribute_slug ] ) ); ?>" aria-label="(Edit)">
                            <?php echo esc_html( $attribute_name ); ?>
                        </a>
                        <div class="row-actions">
                            <span class="edit">
                                <a href="admin.php?<?php echo http_build_query( array_merge( $_GET, [ 'attr' => $attribute_slug ] ) ); ?>" aria-label="Edit">
                                    <?php esc_html_e( 'Edit' ); ?>
                                </a>
                            </span>
                        </div>
                    </td>
                    <td>
                        <?php echo esc_html( $attribute_slug ); ?>
                    </td>
                    <td>
                        <!--suppress HtmlFormInputWithoutLabel -->
                        <input class="aps-input fake-disabled" name="attribute-<?php echo esc_attr( $attribute_slug ); ?>-advanced-selector" disabled type="checkbox"
                            <?php checked( in_array( $attribute_slug, $advanced_selector_attributes, true ) ); ?>>
                    </td>
                    <td class="aps-reorder-handle"></td>
                    <td>
                        <?php echo Data::get_configured_terms_count( $attribute_slug ); ?>
                        /
                        <?php echo Data::get_terms_count( $attribute_slug ); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <div class="aps-row aps-description-row">
            <div class="aps-column mt-30">
                <label>
                    <span>1 - <?php esc_html_e( 'Allow attributes specified order to have effect (instead of using the default (custom per-product) order):', 'aps' ); ?></span>
                    <input class="aps-input" name="use-specified-order" type="checkbox"
                        <?php checked( Advanced_Product_Selector::$options->get( 'aps-setting-use-specified-order' ), '1' ); ?>>
                </label>
            </div>
        </div>

        <hr>
        <div class="aps-container">
            <div class="aps-row mb-15 aps-column-mobile">
                <label>
                    <input class="aps-input" name="select-attributes-step-by-step" type="checkbox"
                        <?php checked( Advanced_Product_Selector::$options->get( 'aps-setting-select-attributes-step-by-step' ), '1' ); ?>>
                    <span class="p-like"><?php esc_html_e( 'Select attribute step by step', 'aps' ); ?></span>
                </label>
                <!--suppress HtmlFormInputWithoutLabel -->
                <textarea class="ml-20 m-mt-15 m-mb-15" name="attributes-step-by-step-selection-text"
                          placeholder="<?php esc_html_e( 'Text to show when the visitor needs to select attributes step by step' ); ?>"
                ><?php echo esc_attr( Advanced_Product_Selector::$options->get( 'aps-setting-attributes-step-by-step-selection-text' ) ); ?></textarea>
            </div>
            <div class="aps-row mb-15">
                <label>
                    <input class="aps-input" name="automatically-show-the-next-attribute-modal" type="checkbox"
                        <?php checked( Advanced_Product_Selector::$options->get( 'aps-setting-automatically-show-the-next-attribute-modal' ), '1' ); ?>>
                    <span class="p-like"><?php esc_html_e( 'When the user selects an attribute automatically show the next attribute modal (wizard style)', 'aps' ); ?></span>
                </label>
            </div>
            <div class="aps-row mb-15">
                <label>
                    <input class="aps-input" name="show-all-attributes-in-one-modal" type="checkbox"
                        <?php checked( Advanced_Product_Selector::$options->get( 'aps-setting-show-all-attributes-in-one-modal' ), '1' ); ?>>
                    <span class="p-like"><?php esc_html_e( 'Show all attributes stacked in the same modal', 'aps' ); ?></span>
                </label>
            </div>
            <div class="aps-row mb-15">
                <label>
                    <input class="aps-input" name="hide-not-configured-terms" type="checkbox"
                        <?php checked( Advanced_Product_Selector::$options->get( 'aps-setting-hide-not-configured-terms' ), '1' ); ?>>
                    <span class="p-like"><?php esc_html_e( 'Hide not configured terms (without an image)', 'aps' ); ?></span>
                </label>
            </div>
            <div class="aps-row mb-15">
                <label>
                    <input class="aps-input" name="deselect-option-on-the-click" type="checkbox"
                        <?php checked( Advanced_Product_Selector::$options->get( 'aps-setting-deselect-option-on-the-click' ), '1' ); ?>>
                    <span class="p-like"><?php esc_html_e( 'Deselect selected options on click', 'aps' ); ?></span>
                </label>
            </div>
            <div class="aps-row mb-15">
                <label>
                    <span class="p-like"><?php esc_html_e( 'How to treat unavailable combinations of attributes:', 'aps' ); ?></span>
                    <select class="aps-input aps-select-input ml-15 m-mt-15 m-mb-15" name="how-to-treat-unavailable-attribute-combinations">
                        <option value="hide"
                            <?php
                            selected(
                                Advanced_Product_Selector::$options->get( 'aps-setting-how-to-treat-unavailable-attribute-combinations' ),
                                'hide'
                            );
                            ?>
                        >
                            <?php esc_html_e( 'Hide unavailable combinations of attribute terms', 'aps' ); ?>
                        </option>
                        <option value="gray-out"
                            <?php
                            selected(
                                Advanced_Product_Selector::$options->get( 'aps-setting-how-to-treat-unavailable-attribute-combinations' ),
                                'gray-out'
                            );
                            ?>
                        >
                            <?php esc_html_e( 'Grey out unavailable combinations (on select resets other choices)', 'aps' ); ?>
                        </option>
                        <option value="disable"
                            <?php
                            selected(
                                Advanced_Product_Selector::$options->get( 'aps-setting-how-to-treat-unavailable-attribute-combinations' ),
                                'disable'
                            );
                            ?>
                        >
                            <?php esc_html_e( 'Disable unavailable combinations (cannot be selected)', 'aps' ); ?>
                        </option>
                    </select>
                </label>
            </div>
            <div class="aps-row">
                <label>
                    <span class="p-like"><?php esc_html_e( 'Placement of the text for non-available options:', 'aps' ); ?></span>
                    <select class="aps-input aps-select-input ml-15 m-mt-15 m-mb-15" name="non-available-options-text-placement">
                        <option value="below-the-image"
                            <?php
                            selected(
                                Advanced_Product_Selector::$options->get( 'aps-setting-non-available-options-text-placement' ),
                                'below-the-image'
                            );
                            ?>
                        >
                            <?php esc_html_e( 'Below the image', 'aps' ); ?>
                        </option>
                        <option value="over-the-image"
                            <?php
                            selected(
                                Advanced_Product_Selector::$options->get( 'aps-setting-non-available-options-text-placement' ),
                                'over-the-image'
                            );
                            ?>
                        >
                            <?php esc_html_e( 'Over the image', 'aps' ); ?>
                        </option>
                    </select>
                </label>
            </div>
            <div class="aps-column aps-align-items-flex-start mt-10 aps-image-block">
                <div class="aps-column aps-image-container aps-arrow-image-container" data-no-image-property="noCustomArrowForProgressBar">
                    <?php
                    $custom_arrow_for_progress_bar = Advanced_Product_Selector::$options->get( 'aps-setting-custom-progress-bar-arrow' );
                    if (
                        $custom_arrow_for_progress_bar &&
                        $image_html = HTML::get_img( $custom_arrow_for_progress_bar, true )
                    ) :
                        ?>
                        <?php echo $image_html; ?>
                    <?php else : ?>
                        <span class="p-like mt-5 mb-5"><?php esc_html_e( 'No custom arrow for progress bar set.', 'aps' ); ?></span>
                    <?php endif; ?>
                </div>
                <div class="aps-images-block">
                    <?php submit_button( __( 'Set progress bar arrow', 'aps' ), [ 'secondary', 'aps-image-upload' ], 'set-custom-arrow-image' ); ?>
                    <?php submit_button( __( 'Unset', 'aps' ), [ 'delete', 'aps-image-unset' ], 'unset-custom-arrow-image' ); ?>
                </div>
            </div>
        </div>

        <hr>
        <div class="aps-container">
            <div class="aps-column mt-m20 mb-20">
                <?php submit_button( __( 'Save changes', 'aps' ), [ 'primary', 'aps-submit' ] ); ?>
            </div>
        </div>
        <div class="aps-container">
            <div class="aps-row aps-justify-center">
                <a class="aps-link aps-export-settings-link"><?php esc_html_e( 'Export Settings', 'aps' ); ?></a>
                <a class="aps-link aps-import-settings-link"><?php esc_html_e( 'Import Settings', 'aps' ); ?></a>
                <input type="file" name="aps-import-settings-link-file-input" accept="application/json, application/zip">
            </div>
            <div class="aps-row aps-justify-center">
                <a class="aps-link aps-reset-settings-link mt-15"><?php esc_html_e( 'Reset All Settings', 'aps' ); ?></a>
            </div>
        </div>
    </form>
</div>
