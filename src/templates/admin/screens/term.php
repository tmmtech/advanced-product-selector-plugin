<?php
/**
 * Template for admin menu, for attribute term.
 *
 * @package Advanced-Product-Selector
 * @since   0.0.1
 */

use Advanced_Product_Selector\Advanced_Product_Selector;
use Advanced_Product_Selector\Data;
use Advanced_Product_Selector\HTML;

$attr_slug = $_GET['attr'];

$taxonomy         = "pa_$attr_slug";
$terms            = get_terms( $taxonomy, [ 'hide_empty' => false ] );
$term_names       = array_column( $terms, 'name', 'slug' );
$term_ids         = array_column( $terms, 'term_id', 'slug' );
$term_slug        = $_GET['term'];
$term_id          = $term_ids[ $term_slug ];
$term_name        = $term_names[ $term_slug ];
$term_data        = Data::get_term_data( $term_id );
$image_data       = Data::get_image_data( $term_data['image'], 'thumbnail' );
$image_hover_data = Data::get_image_data( $term_data['image-hover'], 'thumbnail' );

?>

<div class="wrap">
    <h1 class="aps-header">
        <?php echo str_replace( '{}', $term_names[ $term_slug ], esc_html__( 'Configure term "{}"', 'aps' ) ); ?>
    </h1>

    <form data-term-id="<?php echo $term_id; ?>" data-taxonomy="<?php echo $taxonomy; ?>">
        <div class="aps-attribute-term-container">
            <h2>
                <?php esc_html_e( 'Description', 'aps' ); ?>
            </h2>
            <div class="aps-column">
                <?php
                $editor_id = 'term-description';
                wp_editor(
                    $term_data['description'],
                    $editor_id,
                    [
                        'textarea_name' => $editor_id,
                        'textarea_rows' => 5,
                    ]
                );
                ?>
            </div>

            <h2 class="mt-30">
                <?php esc_html_e( 'Tooltip', 'aps' ); ?>
            </h2>
            <div class="aps-column">
                <?php
                $editor_id = 'term-tooltip';
                wp_editor(
                    $term_data['tooltip'],
                    $editor_id,
                    [
                        'textarea_name' => $editor_id,
                        'textarea_rows' => 5,
                    ]
                );
                ?>
            </div>

            <h2 class="mt-30">
                <?php esc_html_e( 'Recommended term - text', 'aps' ); ?>
            </h2>
            <div class="aps-column">
                <?php
                $editor_id = 'term-recommended-text';
                wp_editor(
                    $term_data['recommended_text'],
                    $editor_id,
                    [
                        'textarea_name' => $editor_id,
                        'textarea_rows' => 5,
                    ]
                );
                ?>
            </div>

            <h2 class="mt-30">
                <?php esc_html_e( 'Recommended term - border color', 'aps' ); ?>
            </h2>
            <span class="aps-color-select" id="aps-recommended-border-color"
                  data-color="<?php echo esc_attr( $term_data['recommended_border_color'] ); ?>" data-data-name="recommendedBorderColor"></span>

            <div class="aps-image-block">
                <h2 class="mt-30">
                    <?php esc_html_e( 'Image', 'aps' ); ?>
                </h2>
                <div class="aps-column aps-image-container mb-m10">
                    <?php if ( $image_data ) : ?>
                        <?php echo HTML::get_img( $term_data['image'], true ); ?>
                    <?php else : ?>
                        <?php esc_html_e( 'No image added yet.', 'aps' ); ?>
                    <?php endif; ?>
                </div>
                <div class="aps-images-block">
                    <?php submit_button( __( 'Set image', 'aps' ), [ 'secondary', 'aps-image-upload' ], 'set-image' ); ?>
                    <?php submit_button( __( 'Unset image', 'aps' ), [ 'delete', 'aps-image-unset' ], 'unset-image' ); ?>
                </div>
            </div>

            <div class="aps-image-block">
                <h2 class="mt-30">
                    <?php esc_html_e( 'On Hover Image', 'aps' ); ?>
                </h2>
                <div class="aps-column aps-image-container aps-image-hover-container mb-m10">
                    <?php if ( $image_hover_data ) : ?>
                        <?php echo HTML::get_img( $term_data['image-hover'], true ); ?>
                    <?php else : ?>
                        <?php esc_html_e( 'No image added yet.', 'aps' ); ?>
                    <?php endif; ?>
                </div>
                <div class="aps-images-block">
                    <?php submit_button( __( 'Set image', 'aps' ), [ 'secondary', 'aps-image-upload' ], 'set-hover-image' ); ?>
                    <?php submit_button( __( 'Unset image', 'aps' ), [ 'delete', 'aps-image-unset' ], 'unset-hover-image' ); ?>
                </div>
            </div>

            <hr class="mt-15 mb-40">
            <div class="aps-container">
                <div class="aps-column mt-m20 mb-20">
                    <?php submit_button( __( 'Save changes', 'aps' ), [ 'primary', 'aps-submit' ] ); ?>
                    <p>
                        <a href="admin.php?page=aps&attr=<?php echo $attr_slug; ?>">
                            <- <?php esc_html_e( 'Go to attribute', 'aps' ); ?>
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </form>
</div>
