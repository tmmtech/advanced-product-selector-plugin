<?php
/**
 * Admin Menus
 *
 * Adds admin menus.
 *
 * @package Advanced-Product-Selector
 * @since   0.0.1
 */

namespace Advanced_Product_Selector;

/**
 * Class Admin_Menu.
 */
final class Admin_Menu {

    /**
     * Adds the menu and inits assets loading for it.
     */
    public function __construct() {
        add_action( 'admin_menu', [ $this, 'init_menu' ] );
    }

    /**
     * Adds the menu and inits assets loading for it.
     */
    public function init_menu() {
        if ( array_key_exists( 'attr', $_GET ) && array_key_exists( 'term', $_GET ) ) {
            $menu_slug = add_submenu_page(
                'woocommerce',
                __( 'Advanced Product Selector', 'aps' ),
                __( 'Advanced Product Selector', 'aps' ),
                'manage_options',
                'aps',
                function () {
                    require_once ADVANCED_PRODUCT_SELECTOR_DIR . 'src/templates/admin/screens/term.php';
                }
            );

            add_action(
                'load-' . $menu_slug,
                [ 'Advanced_Product_Selector\Assets\Menu\Screens\Term', 'init' ]
            );
        } elseif ( array_key_exists( 'attr', $_GET ) ) {
            $menu_slug = add_submenu_page(
                'woocommerce',
                __( 'Advanced Product Selector', 'aps' ),
                __( 'Advanced Product Selector', 'aps' ),
                'manage_options',
                'aps',
                function () {
                    require_once ADVANCED_PRODUCT_SELECTOR_DIR . 'src/templates/admin/screens/attr.php';
                }
            );

            add_action(
                'load-' . $menu_slug,
                [ 'Advanced_Product_Selector\Assets\Menu\Screens\Attr', 'init' ]
            );
        } else {
            $menu_slug = add_submenu_page(
                'woocommerce',
                __( 'Advanced Product Selector', 'aps' ),
                __( 'Advanced Product Selector', 'aps' ),
                'manage_options',
                'aps',
                function () {
                    require_once ADVANCED_PRODUCT_SELECTOR_DIR . 'src/templates/admin/screens/main.php';
                }
            );

            add_action(
                'load-' . $menu_slug,
                [ 'Advanced_Product_Selector\Settings_Export', 'add_listeners' ]
            );

            add_action(
                'load-' . $menu_slug,
                [ 'Advanced_Product_Selector\Assets\Menu\Screens\Main', 'init' ]
            );
        }
    }

    /**
     * Returns an array of texts for the admin scripts.
     */
    public static function get_texts() {
        return [
            'error'                          => __( 'Error', 'aps' ),
            'somethingWentWrong'             => __( 'Something went wrong.', 'aps' ),
            'areYouSure'                     => __( 'Are you sure?', 'aps' ),
            'yes'                            => __( 'Yes', 'aps' ),
            'no'                             => __( 'No', 'aps' ),
            'resetAllSettingsNotice'         => __( 'Are you sure you want to reset all plugin settings?', 'aps' ),
            'yesResetAllSettings'            => __( 'Yes, reset all plugin settings!', 'aps' ),
            'productSelector'                => __( 'Product Selector', 'aps' ),
            'noImageAddedYet'                => __( 'No image added yet.', 'aps' ),
            'noCustomArrowForProgressBarSet' => __( 'No custom arrow for progress bar set.', 'aps' ),
            'exportPopupTitle'               => __( 'Do you want to export images?', 'aps' ),
            'exportPopupText'                => __( 'When you answer no, the images will be downloaded by their URLs upon import. Answer yes if the site is not reachable via the internet, or if you want to export the settings as a backup.', 'aps' ),
            /* translators: %s is a plugin name. */
            'settingsAreImporting'           => sprintf( __( '%s settings are being imported.', 'aps' ), Advanced_Product_Selector::$name ),
            'pleaseWait'                     => __( 'Please wait.', 'aps' ),
        ];
    }
}
